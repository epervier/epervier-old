-- loveutils.table : simple functions for table manipulation and computation.

--[[
  Copyright © 2021 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Table = {}
local Bools = require "framework.utils.bools"

--- Get the sum of a liste of number
---@param table table the table which you want to find if it contain the content
---@param content any the content that you want to find in the table
---@return boolean contain if the table contain the content
function Table.contain(table, content)
  for k, v in ipairs(table) do
    if (v == content) then
      return true
    end
  end
  return false
end

--- Get the table in form of a string
---@param table table the table which you want to transform into a string
---@return string string the string created from the table
function Table.toString(table, depth)
  depth = depth or 2
  local string = "{"
  for key, value in pairs(table) do
    if (type(value) ~= "userdata" and depth > 0) then
      if (type(value) == "table") then
        if (value.is ~= nil) then
          string = string .. "Object"
        end
        string = string .. Table.toString(value, depth - 1)
      elseif (type(value) == "boolean") then
        string = string .. Bools.toString(value)
      else
        string = string .. value
      end
      string = string .. ","
    end
  end
  return string .. "}"
end

--- Clone a table
---@param table1 table the table to clone
---@return table returnTable the cloned table
function Table.clone(table1)
  local returnTable = {}
  for key, value in pairs(table1) do
    returnTable[key] = value
  end
  return returnTable
end

--- Merge two list
---@param table1 table the first list to merge
---@param table2 table the list that you want to merge to the first
---@return table table1 the first list, merged with the second
function Table.mergeList(table1, table2)
  for i, value in ipairs(table2) do
    table.insert(table1, value)
  end
  return table1
end

--- Merge two table
---@param table1 table the first table to merge
---@param table2 table the table that you want to merge to the first
---@return table table1 the first table, merged with the second
function Table.merge(table1, table2)
  for key, value in pairs(table2) do
    table1[key] = value
  end
  return table1
end

--- Reduce a list with a function
---@param list table the list you want to reduce
---@param fn function a function to apply to the whole list. Args: the first reduced list & the current value
---@return any acc the result of the reducing
function Table.reduce(list, fn)
  local acc
  for k, v in ipairs(list) do
      if 1 == k then
          acc = v
      else
          acc = fn(acc, v)
      end
  end
  return acc
end

--- Get the sum of a liste of number
---@param table table the list to parse into an sum
---@return number sum the sum of the table
function Table.sum(table)
  local sum = 0
  for _, v in pairs(table) do
    sum = sum + v
  end

  return sum
end

--- Get the average of a liste of number
---@param table table the list to parse into an average
---@return number average the average of the table
function Table.average(table)
  return Table.sum(table) / #table
end

--- Parse a basic list into a structured table. Return an error if the number of arguments is not the same
---@param table table the list to parse into a table
---@param structure table the structure to create the table : each name correspond to an attribute of the parsed table
---@param nullableNbr integer the number of nullable argument (at the end of the functions) (can be null)
---@return table parsedTable the parsed table
function Table.parse(table, structure, nullableNbr)
  local parsedTable = {}
  assert(table ~= nil, "The table to parse can't be null")
  assert(structure ~= nil, "The table structure can't be null")
  nullableNbr = nullableNbr or 0

  if ((#table) > (#structure)) or ((#table) < (#structure - nullableNbr)) then
    error("The table to parse " .. Table.toString(table) ..
      " doesn't have the right number of arguments: " .. #table ..
      " instead of " .. #structure ..
      " and " .. nullableNbr .. " nullables")
  else
    for i, key in ipairs(structure) do
      parsedTable[key] = table[i]
    end
  end

  return parsedTable
end

function Table.remove(table, funcDelete)
  -- Code from https://stackoverflow.com/questions/12394841/safely-remove-items-from-an-array-table-while-iterating
  local j, n = 1, #table;

  for i=1,n do
      if (not funcDelete(table[i], i, j)) then
          -- Move i's kept value to j's position, if it's not already there.
          if (i ~= j) then
              table[j] = table[i];
              table[i] = nil;
          end
          -- Increment position of where we'll place the next kept value.
          j = j + 1;
      else
          table[i] = nil;
      end
  end

  return table;
end

return Table
