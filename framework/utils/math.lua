--- loveutils.math : easy to use functions for mathematics and geometry.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Math = {}

-- ALGEBRA FUNCTIONS
-- Simple yet usefull functions not supported by base love2D

function Math.sign(x)
   if (x < 0) then
     return -1
   elseif (x > 0) then
     return 1
   else
     return 0
   end
end

function Math.round(num)
  return math.floor(num + 0.5)
end

function Math.toZero(num, sub)
  local sub = math.floor(sub)

  if math.abs(num) < sub then
    return 0
  else
    return num - (sub * Math.sign(num))
  end
end

function Math.between(num, value1, value2)
  local min = math.min(value1, value2)
  local max = math.max(value1, value2)
  return math.min(math.max(num, min), max)
end

function Math.either(bool, val1, val2)
  if (bool) then
    return val1
  else
    return val2
  end
end

function Math.wrap(val, min, max)
  while (val < min) do
    local diff = ((min-1) - val)
    val = max - diff
  end
  while (val > max) do
    local diff = ((max+1) - val)
    val = min + diff
  end
  return val
end

-- VECTOR/DIRECTION functions
-- Easy-to-use function to handle point and motion

function Math.vector(x1, y1, x2, y2)
  local vecx, vecy

  vecx = x2 - x1
  vecy = y2 - y1

  return vecx, vecy
end

function Math.getMiddlePoint(x1, y1, x2, y2)
  local newx, newy, vecx, vecy

  vecx = math.max(x1, x2) - math.min(x1, x2)
  vecy = math.max(y1, y2) - math.min(y1, y2)

  newx = math.min(x1, x2) + (vecx / 2)
  newy = math.min(y1, y2) + (vecy / 2)

  return newx, newy
end

function Math.pointDistance(x1, y1, x2, y2)
  local vecx, vecy

  vecx = math.max(x1, x2) - math.min(x1, x2)
  vecy = math.max(y1, y2) - math.min(y1, y2)

  return math.sqrt(vecx^2 + vecy^2)

end

function Math.pointDirection(x1,y1,x2,y2)
  local vecx, vecy, angle
  vecy = y2 - y1
  vecx = x2 - x1
  -- We disable diagnostic to this line as we use LuaJIT 2.0
  ---@diagnostic disable-next-line: deprecated
  angle = math.atan2(-vecy, vecx)

  return angle
end

function Math.lengthdir(lenght, dir)
  local x = math.cos(dir) * lenght
  local y = -math.sin(dir) * lenght
  return x, y
end

-- 3D MATH FUNCTIONS
-- Basic math calculations for 3D

function Math.getMiddlePoint3D(x1, y1, z1, x2, y2, z2)
  local newx, newy, newz, vecx, vecy, vecz

  vecx = math.max(x1, x2) - math.min(x1, x2)
  vecy = math.max(y1, y2) - math.min(y1, y2)
  vecz = math.max(z1, z2) - math.min(z1, z2)

  newx = math.min(x1, x2) + (vecx / 2)
  newy = math.min(y1, y2) + (vecy / 2)
  newz = math.min(z1, z2) + (vecz / 2)

  return newx, newy, newz
end

function Math.pointDistance3D(x1, y1, z1, x2, y2, z2)
  local vecx, vecy, vecz

  vecx = math.max(x1, x2) - math.min(x1, x2)
  vecy = math.max(y1, y2) - math.min(y1, y2)
  vecz = math.max(z1, z2) - math.min(z1, z2)

  return math.sqrt(vecx^2 + vecy^2 + vecz^2)

end

-- STRING FUNCTIONS
-- Transform into string numbers

function Math.numberToString(x, length)
  local length = length or 1
  local string = ""
  local x = x
  if (x >= (10 ^ length)) then
    string = string .. x
  else
    for i=1, (length-1) do
      if (x < (10 ^ (length-i))) then
        string = string .. "0"
      end
    end
    string = string .. x
  end
  return string
end

-- COORDINATE FUNCTIONS
-- Easy computation on coordinate

function Math.floorCoord(x, y)
  return math.floor(x), math.floor(y)
end

function Math.pixeliseCoord(x, y, factor)
  x, y = Math.floorCoord(x / factor, y / factor)

  x = x * factor
  y = y * factor

  return x, y
end

return Math
