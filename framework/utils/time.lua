local Math = require "framework.utils.math"

Time = {}

Time.HOURS = 3600
Time.MINUTES = 60
Time.SEPARATOR = ":"

function Time.timestamp(hour, minute, seconds)
    return (hour * Time.HOURS) + (minute * Time.MINUTES) + seconds
end

function Time.getFields(timestamp)
    local hours = math.floor(timestamp / Time.HOURS)
    local minutes = math.floor((timestamp % Time.HOURS) / Time.MINUTES)
    local seconds = math.floor((timestamp % Time.MINUTES))
    return hours, minutes, seconds
end

function Time.toString(timestamp)
    local hours, minutes, seconds = Time.getFields(timestamp)
    local str = Math.numberToString(hours, 2)
    str = str .. Time.SEPARATOR .. Math.numberToString(minutes, 2)
    str = str .. Time.SEPARATOR .. Math.numberToString(seconds, 2)
    return str
end

return Time