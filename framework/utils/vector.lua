local vectors = {}
local Vector = require "framework.libs.brinevector3D"

---@class Vector
---@field x number the x coordinate of the vector
---@field y number the y coordinate of the vector
---@field z number the z coordinate of the vector

--- Apply a function to all coordinate of a vector to get a new one
---@param vector Vector the vector you want to apply to
---@param func function the function you want to apply
---@return Vector vector a new vector created from the first one and the function
function vectors.applyFunctionToCoord(vector, func)
    return Vector(func(vector.x), func(vector.y), func(vector.z))
end



--- Get a vector with all coordinate turned to the largest integral value smaller than or equal to them
---@param vector Vector the vector you want to get the floor coordinate
---@return Vector vector the new vector with floor-cordinates
function vectors.floor(vector)
    return vectors.applyFunctionToCoord(vector, math.floor)
end

--- Get a vector with all coordinate turned to the smallest integral value larger than or equal to them
---@param vector Vector the vector you want to get the ceil coordinate
---@return Vector vector the new vector with ceil-cordinates
function vectors.ceil(vector)
    return vectors.applyFunctionToCoord(vector, math.ceil)
end

return vectors