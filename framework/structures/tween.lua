return {
    tween = {"name", "type", "start", "duration", "target", "easing"},
    movement = {"name", "type", "start", "duration", "x", "y", "easing"},
    switch = {"name", "type", "start", "bools"},
    delayFocus = {"name", "type", "start"},
}