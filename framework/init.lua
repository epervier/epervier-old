---@diagnostic disable: duplicate-set-field
-- framework :: The main Épervier Framework script, loading the core and main 
-- utilities

--[[
  Copyright © 2021 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

require "framework.globals"

framework = {}

-- GLOBAL UTILS/FUNCTION LOADING
-- Load in the global namespace utilities that'll need to be reusable everywhere
-- in the game

Object  = require("framework.libs.classic")
utils   = require("framework.utils")
enum    = require("framework.utils.enum")

Assets = require("framework.assets")

framework.Core = require("framework.core")

function framework.start(gamemodule, args)
  framework.startCore(args)
  if (gamemodule ~= nil) then
    framework.startGame(gamemodule)
  end

  assets = Assets(true)
end

function framework.startCore(args)
  core = framework.Core(args)
end

function framework.startGame(gamemodule)
  local GameObject = require(gamemodule)
  game = GameObject()
end

require("framework.callbacks")