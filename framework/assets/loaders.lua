local loaders = {}

local Sprite = require "framework.assets.type.sprite"
local Tileset = require "framework.assets.type.tileset"

function loaders.sfx(list, id, data)
    list[id] = love.audio.newSource(data.path, "static")
end

function loaders.textures(list, id, data)
    list[id] = love.graphics.newImage(data.path)
end

function loaders.music(list, id, data)
    list[id] = love.audio.newSource(data.path, "stream")
end

function loaders.fonts(list, id, data)
    local metadata = require ("assets.fonts." .. id)
    if (data.extension == "ttf") then
        list[id] = love.graphics.newFont(data.path, metadata.default)
        if (metadata.sizes ~= nil) then
            for i, size in ipairs(metadata.sizes) do
                list[id .. "." .. size] = love.graphics.newFont(data.path, size)
            end
        end
    else
        list[id] = love.graphics.newImageFont(data.path, metadata.glyphs, metadata.extraspacing or 1)
    end
end

function loaders.sprites(list, id, data)
    local spriteData = require ("assets.sprites." .. id)
    list[id] = Sprite(data.path, spriteData)
end

function loaders.tilesets(list, id, data)
    local tilesetData = require ("assets.sprites." .. id)
    list[id] = Tileset(data.path, tilesetData)
end

return loaders