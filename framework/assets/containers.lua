-- assets/containers/parent.lua :: A basic asset container

--[[
  Copyright © 2024 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local AssetContainer = Object:extend()

function AssetContainer:new(assetType, checkFor, loader, preload)
    self.type = assetType
    self.preload = preload
    self.checkFor = checkFor
    self.loader = loader
    self.pathes = {}
    self.list = {}
    self:scanFolder(nil, nil)
end

function AssetContainer:scanFolder(folder, parent)
  local prefix = "assets/" .. self.type
  local folder = folder or ""
  local parent = parent or ""

  local path = prefix .. "/" .. folder

  local files = love.filesystem.getDirectoryItems( path ) 
  for i, file in ipairs(files) do
    local filepath = path .. file

    if (love.filesystem.getInfo( filepath ).type == "file" ) then
      local data = love.filesystem.newFileData( filepath )
      local extension = data:getExtension()
      local fileName = string.sub(file, 1, #file - #extension - 1)

      if (utils.table.contain(self.checkFor, extension)) then
        self.pathes[parent .. fileName] = {path = filepath, extension = extension, canonical = path .. fileName}
        if (self.preload) then
          self:load(parent .. fileName)
        end
      end

    else
      self:scanFolder(folder .. file .. "/", parent .. file .. ".")
    end
  end
end

function AssetContainer:load(id)
  assert(id ~= nil, "AssetContainer:load shoudln't be called with a nil id")
  assert(id ~= "", "AssetContainer:load shoudln't be called with an empty id")
  assert(self.pathes[id] ~= nil, "Asset " .. self.type .. " " .. id .. " doesn't exist")
  if (self.list[id] == nil) then
    self.loader(self.list, id, self.pathes[id])
  end
end

function AssetContainer:get(id)
  self:load(id)
  return self.list[id]
end

return AssetContainer