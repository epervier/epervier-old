-- assets/init.lua :: The main file of the asset manager, an object that'll handle
-- all assets of the framework. It's disconnected of screens, but can handle some assets
-- from their caller in lazyloading mode

--[[
  Copyright © 2024 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local AssetManager = Object:extend()
local AssetContainer = require "framework.assets.containers"

local loaders = require "framework.assets.loaders"

function AssetManager:new(preload)
    self.sfx = AssetContainer("sfx", {"mp3", "ogg", "wav"}, loaders.sfx, preload)
    self.textures = AssetContainer("textures", {"png", "jpg", "webp"}, loaders.textures, preload)
    self.music = AssetContainer("music", {"mp3", "ogg", "wav"}, loaders.music, false)
    self.fonts = AssetContainer("fonts", {"png", "ttf"}, loaders.fonts, preload)
    self.sprites = AssetContainer("sprites", {"png"}, loaders.sprites, preload)
end

function AssetManager:playSFX(id)
    local sfx = self.sfx:get(id)
    sfx:stop()
    sfx:setVolume(core.options.data.audio.sfx / 100)
    love.audio.play(sfx)
end

function AssetManager:stopSFX(id)
    local sfx = self.sfx:get(id)
    sfx:stop()
end

function AssetManager:setFont(font)
    love.graphics.setFont(self.fonts:get(font))
end

function AssetManager:print(font, text, x, y, align, r, sx, sy, ox, oy, kx, ky)
    self:setFont(font)
    utils.graphics.print(text, x, y, align, r, sx, sy, ox, oy, kx, ky)
end

function AssetManager:printf(font, text, x, y, limit, align, r, sx, sy, ox, oy, kx, ky)
    self:setFont(font)
    if (limit > 0) then
        love.graphics.printf(text, x, y, limit, align, r, sx, sy, ox, oy, kx, ky)
    else
        utils.graphics.print(text, x, y, align, r, sx, sy, ox, oy, kx, ky)
    end
end

return AssetManager