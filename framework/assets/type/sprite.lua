-- assets/sprite :: the assets object, which is basically a tileset with animation
-- and frame metadata

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Sprite    = Object:extend()

local Tileset   = require "framework.assets.type.tileset"

-- INIT FUNCTIONS
-- Initilizing and configuring option

function Sprite:new(filepath, data)
  self.tileset  = Tileset(filepath, data)
  self.data = data
end

-- INFO FUNCTIONS
-- get information with these functions

function Sprite:getDefaultAnimation()
    return self.data.metadata.defaultAnim
end

function Sprite:getAbsoluteFrame(animation, subframe)
    return self.data.animations[animation].startAt + subframe
end

function Sprite:getAnimationData(animation)
    return self.data.animations[animation]
end

function Sprite:getAnimationLenght(animation)
    local animationData = self:getAnimationData(animation)

    if (animationData.lenght ~= nil) then
        return animationData.lenght
    else
        return (animationData.endAt - animationData.startAt)
    end
end

function Sprite:getAnimationSpeed(animation, custom)
    local animationData = self:getAnimationData(animation)

    if (animationData.speed ~= nil and animationData.speed >= 0) then
        return animationData.speed
    end

    return custom or 0
end

function Sprite:isAnimationOver(animation, frame)
    return frame >= self:getAnimationLenght(animation)
end

function Sprite:getAnimationRestartFrame(animation)
    local animationData = self:getAnimationData(animation)

    if (animationData.pauseAtEnd == true) then
        return self:getAnimationLenght() - 1
    end

    if (animationData.restart ~= nil) then
        return (animationData.restart)
    elseif (animationData.restartAt ~= nil) then
        return (animationData.restartAt - animationData.startAt)
    elseif (animationData.loop ~= nil) then
        return (animationData.loop - animationData.startAt)
    else
        return 0
    end
end

function Sprite:getAnimationDuration(animation)
    local animationData = self:getAnimationData(animation)
    return (self:getAnimationLenght(animation)) / animationData.speed
end

function Sprite:animationExist(name)
  return (self:getAnimationData(name) ~= nil)
end

function Sprite:getFrameSignal(animation, frame)
    local animationData = self:getAnimationData(animation)

    if (animationData.signal == nil or type(animationData.signal ~= "table")) then
        return {}
    end

    local signals = {}
    if (type(animationData.signal[1]) ~= "table" and animationData.signal[1] == frame) then
        -- Si le premier élément de la liste signal n'est pas une table, cela veut dire que le signal est de la forme {frame, signal}
        table.insert(signals, animationData.signal[2])
    else
        -- Sinon cela veut dire que le signal est de la forme {{frame1, signal1}, {frame2, signal2}}
        for _, signal in ipairs(animationData.signal) do
            if (signal[1] == frame) then
                table.insert(signals, signal[2])
            end
        end
    end

    return signals
end

function Sprite:getDimensions()
  return self.tileset:getDimensions()
end

function Sprite:getAreas(animation, frame)
    local animationData = self:getAnimationData(animation)
    if (animationData.areas == nil) then
        return nil
    end
    local frameList = nil
    for i = 1, (frame+1), 1 do
        local frameData = animationData.areas["frame" .. i]
        if (frameData ~= nil) then
            frameList = frameData
        end
    end
    return frameList
end

-- DRAW FUNCTIONS
-- Draw sprites using these functions

function Sprite:draw(animation, frame, x, y, r, sx, sy, ox, oy, kx, ky)
    self.tileset:drawTile(self:getAbsoluteFrame(animation, frame), x, y, r, sx, sy, ox, oy, kx, ky)
end

function Sprite:drawPart(animation, frame, x, y, w, h, r, sx, sy, ox, oy, kx, ky)
  local w = math.floor(w)
  local h = math.floor(h)

  if w >= 0 and h <= 0 then
    return 0
  end

  love.graphics.setScissor(x - ox, y - oy, w, h)
  self:draw(animation, frame, x, y, r, sx, sy, ox, oy, kx, ky)
  love.graphics.setScissor( )
end

return Sprite
