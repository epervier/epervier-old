-- GameSystem :: The main GameSystem subsystem. Basically a big object that handle all the
-- GameSystem-related data like characters, monsters, etc. While the core aim to be
-- reusable at will, the GameSystem is specifically made for the current GameSystem. 

--[[
  Copyright © 2019 Kazhnuz
  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]


local Serializer  = require "framework.classes.serializable.serializer" 
local GameSystem = Serializer:extend()

local VAR_TO_SERIALIZE = {
    "gametime",
    "destroyedGizmo",
    "variables",
    "flags"
} 

function GameSystem:new()
    self.slot = -1
    self.slotNumber = 3
    self.version = core.conf.gameversion or "N/A"
    self:reset()
    GameSystem.super.new(self, VAR_TO_SERIALIZE)
end

function GameSystem:reset()
    self.gametime = 0

    self.flags = {}
    self.destroyedGizmo = {}
    self.variables = {}
    self.mapName = ""
end

function GameSystem:reload()
    self:read(self.slot)
end
  
function GameSystem:read(save_id)
    self.slot = save_id
    if (self.slot > 0) then
      self:deserialize(self:getSaveName())
    end
end
  
function GameSystem:deleteCurrentSave()
    if (self.slot > 0) then
      self:delete(self:getSaveName())
      self.metadata:remove(self.slot)
    end
end
  
function GameSystem:write()
    if (self.slot > 0) then
      self:serialize(self:getSaveName())
    end
end
  
function GameSystem:getSaveName(saveslot)
    local saveslot = saveslot or self.slot
    return "save" .. saveslot .. ".save"
end
  
function GameSystem:resetSaves()
    for i=1, self.slotNumber do
      self:delete(self:getSaveName(i))
    end
end
  
function GameSystem:update(dt)
    self.gametime = self.gametime + dt
end
  
function GameSystem:getTime()
    return utils.time.getFields(self.gametime)
end
  
function GameSystem:getTimeString() 
    return utils.time.toString(self.gametime)
end
  
function GameSystem:printTime()
    core.debug:print(self:getTimeString())
end

return GameSystem