-- core/options.lua :: The options loading/saving system. Is used by the other
-- modules to save their settings.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]
local Serializer = require "framework.classes.serializable.serializer"
local OptionsManager = Serializer:extend()

local TRANSLATION_PATH = "datas/languages/"

local OPTION_FILE = "options.data"

-- INIT FUNCTIONS
-- Initialize and configure the game options

function OptionsManager:new(controller)
  OptionsManager.super.new(self, {"data"})

  self.controller = controller
  -- We begin by creating an empty data table before reading the data.
  self:reset()
  self:read()
end

function OptionsManager:reset()
  local conf = self.controller:getDefaultConf()
  -- Reset the option to the game defaults.
  self.data = {}

  self.data.video = {}
  self.data.video.crtfilter   = (conf.window.crtfilter == true)
  self.data.video.resolution  = conf.window.resolution or 1
  self.data.video.border      = (conf.window.borderless == false)
  self.data.video.vsync       = (conf.window.vsync ~= false)
  self.data.video.fullscreen  = (conf.window.fullscreen == true)

  -- We load the default files
  self.data.input     = self:getInputDefaultData()

  -- TODO: have a way to auto-load a language according to the OS ?
  self.data.language  = self:getTranslationDefaultData()

  self.data.audio = {}
  self.data.audio.music = conf.volume.music or 100
  self.data.audio.sfx   = conf.volume.sfx or 100
end

-- INFO FUNCTIONS
-- Get informations from the option managers

function OptionsManager:getInputDefaultData()
  local _path = "datas/inputs.lua"
  local datas = {}
  local fileinfo = love.filesystem.getInfo(_path)

  if fileinfo ~= nil then
    datas = require "datas.inputs"
  else
    datas = {}
  end

  return datas
end

function OptionsManager:getPlayerInputData(id)
  local _playerInputData = self.data.input[id]

  if _playerInputData == nil then
    _playerInputData      = {}
    _playerInputData.keys = {}
  end

  return _playerInputData
end

function OptionsManager:getInputData()
  return self.data.input
end

function OptionsManager:setInputKey(sourceid, padkey, key)
  if self.data.input[sourceid] ~= nil then
    if self.data.input[sourceid].keys[padkey] ~= nil then
      self.data.input[sourceid].keys[padkey] = key
    end
  end
end

-- Lang data

function OptionsManager:getTranslationDefaultData()
  local _path = TRANSLATION_PATH .. "init.lua"
  local fileinfo = love.filesystem.getInfo(_path)
  local datas = nil
  local lang
  if fileinfo ~= nil then
    lang = require(TRANSLATION_PATH)
    lang.current  = lang.default or "en"
    lang.path     = TRANSLATION_PATH
  end

  return lang
end

function OptionsManager:setLanguage(lang)
  if (self.controller.lang:isLangAvailable(lang)) then
    self.data.language.current = lang
    self.controller.lang:getTranslationData()
  end
end

-- DATA HANDLING FUNCTIONS
-- Save and get data from the savefile

-- FIXME: maybe subclass a special module for that ?

function OptionsManager:write()
  self:serialize(OPTION_FILE)
end

function OptionsManager:read()
  self:deserialize(OPTION_FILE)
end

return OptionsManager
