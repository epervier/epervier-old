-- core/screen.lua :: Basic screen manager. Use CScreen as a backend, and works
-- as an abstraction layer around CScreen.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local ScreenManager = Object:extend()

local CScreen = require("framework.libs.cscreen")

-- INIT FUNCTIONS
-- Initialize and configure the screen manager

function ScreenManager:new(controller)
  self.controller = controller
  self.data       = self.controller.options.data.video
  self.width, self.height = love.graphics.getDimensions()
  self:applySettings()
  CScreen.init(self.width, self.height, true)
  CScreen.setColor(0, 0, 0, 1)

  love.graphics.setDefaultFilter( "nearest", "nearest", 1 )

  self.transition = nil
  self.transitionOut = nil
  self.isOpaque = false
end

function ScreenManager:startTransition(transitionIn, transitionOut, func, x, y)
  self.transition = transitionIn(func, x, y, false)
  self.transitionOut = transitionOut(nil, x, y, true)
  self.isOpaque = true
end

function ScreenManager:transitionOver(fadeOut)
  if (not fadeOut) then
    self.transition = self.transitionOut
  else
    self.transition = nil
    self.isOpaque = false
  end
end

function ScreenManager:isActive()
  return ((self.transition == nil) and (not self.isOpaque))
end

function ScreenManager:applySettings()
  self.data       = self.controller.options.data.video

  local flags = {}
  flags.vsync = self.data.vsync
  flags.borderless = (self.data.border == false)

  love.window.setMode(self.width * self.data.resolution, self.height * self.data.resolution, flags)
  love.window.setFullscreen( self.data.fullscreen )

  local width, height = love.window.getMode()
  CScreen.update(width, height)
end

function ScreenManager:update(dt)
  if (self.transition ~= nil) then
    self.transition:update(dt)
  end
end

-- POINTER FUNCTIONS
-- Translate the pointer according to the screen coordinates

function ScreenManager:project(x, y)
  return CScreen.project(x, y)
end

function ScreenManager:getMousePosition()
  return CScreen.project(love.mouse.getX(), love.mouse.getY())
end

function ScreenManager:getScale()
  return CScreen.getScale()
end

function ScreenManager:getScreenCoordinate(x, y)
  return CScreen.getScreenCoordinate(x, y)
end

-- INFO FUNCTIONS
-- Get screen informations

function ScreenManager:getDimensions()
  return self.width, self.height
end

-- SCISSOR FUNCTIONS
-- Simple scissor functions

function ScreenManager:setScissor(x, y, width, height)
  local x, y = self:getScreenCoordinate(x, y)
  love.graphics.setScissor(x, y, width*self.data.resolution, height*self.data.resolution)
end

function ScreenManager:resetScissor()
  love.graphics.setScissor( )
end

-- DRAW FUNCTIONS
-- Apply draw functions to the scene

function ScreenManager:apply()
  CScreen.apply()
end

function ScreenManager:cease()
  CScreen.cease()
end

function ScreenManager:drawTransition()
  if (self.isOpaque) then
    if (self.transition ~= nil) then
      self.transition:draw()
    else
      love.graphics.setColor(0,0,0,1)
      love.graphics.rectangle("fill",0,0,424,240)
      utils.graphics.resetColor()
    end
  end
end

return ScreenManager
