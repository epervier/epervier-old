-- core/music.lua :: A music system, in order to add some usefull features
-- and to make music not scene dependant

--[[
  Copyright © 2020 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local MusicManager = Object:extend()

function MusicManager:new(core)
  self.core = core

  self.musics = {}
  self.isPlaying = false
  self.playNextMusic = false
end

function MusicManager:update(dt)
  if (self.isPlaying and (self.musics[1] ~= nil)) then
    if (not self.musics[1]:isPlaying()) then
      if (self.playNextMusic and (self.musics[1] ~= nil)) then
        self:skip()
        self:playMusic()
        self.playNextMusic = false
      else
        self.isPlaying = false
      end
    end
  end
end

function MusicManager:getCurrentMusic()
  return self.musics[1]
end

function MusicManager:haveMusic()
  return (self.musics[1] ~= nil)
end

function MusicManager:playMusic()
  if (self:haveMusic()) then
    self.musics[1]:setVolume(self.core.options.data.audio.music / 100)
    love.audio.play( self.musics[1] )
    self.isPlaying = true
  end
end

function MusicManager:isPlaying()
  if (self:haveMusic()) then
    return self.musics[1]:isPlaying( )
  else
    return false
  end
end

function MusicManager:addMusic(filename, loop)
  local music = love.audio.newSource(filename, "stream")
  music:setLooping( loop )
  table.insert(self.musics, music)
end

function MusicManager:silence()
  if (self:haveMusic()) then
    self.musics[1]:stop()
    self.isPlaying = false
  end
end

function MusicManager:skip()
  if (self:haveMusic()) then
    self.musics[1]:stop()
    self.isPlaying = false
    table.remove(self.musics, 1)
  end
end

function MusicManager:purge()
  if (self:haveMusic()) then
    self.musics[1]:stop()
    self.isPlaying = false
    self.musics = {}
  end
end


return MusicManager
