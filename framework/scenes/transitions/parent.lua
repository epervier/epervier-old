local TransitionParent = Object:extend()
local TweenManager = require "framework.classes.time"

function TransitionParent:new(func, ox, oy, fadeOut, easeIn, easeOut, duration, wait)
    self.tween = TweenManager(self)
    self:loadResources()
    self.func = func
    self.ox = ox or 0
    self.oy = oy or 0
    self.fadeOut = fadeOut
    if (self.fadeOut) then
        self.value = 1
        self.tween:newTween(wait, duration, {value = 0}, easeOut)
    else
        self.value = 0
        self.tween:newTween(0, duration, {value = 1}, easeIn)
    end
    self.tween:newTimer(duration + wait, "isOver")
end

function TransitionParent:loadResources()
    --vide par defaut
end

function TransitionParent:update(dt)
    self.tween:update(dt * 1.5)
end

function TransitionParent:timerResponse(timer)
    if (timer == "isOver") then
        if (self.func ~= nil) then
            self.func()
        end
        core.screen:transitionOver(self.fadeOut)
    end
end

function TransitionParent:draw()

end

return TransitionParent