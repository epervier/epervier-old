local TransitionParent = require "framework.scenes.transitions.canvas"
local DecalTransition = TransitionParent:extend()

function DecalTransition:new(func, ox, oy, fadeOut, decal)
    self.decal = decal
    DecalTransition.super.new(self, func, ox, oy, fadeOut, "inQuad", "outQuad", 0.8, 0.1)
end

function DecalTransition:loadResources()
    self.decalDrawable = love.graphics.newImage("assets/transitions/" .. self.decal .. ".png")
end

function DecalTransition:drawCanvas()
    love.graphics.setColor(0,0,0,1)
    love.graphics.rectangle("fill", 0, 0, 424, 240)
    utils.graphics.resetColor()
    local w, h = self.decalDrawable:getDimensions()
    local value = (1 - self.value) * (240 / h) * 4
    love.graphics.draw(self.decalDrawable,self.ox,self.oy,0,value, value, w / 2, h / 2)
end

return DecalTransition