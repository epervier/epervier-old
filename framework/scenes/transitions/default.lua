local TransitionParent = require "framework.scenes.transitions.parent"
local DefaultTransition = TransitionParent:extend()

function DefaultTransition:new(func, ox, oy, fadeOut)
    DefaultTransition.super.new(self, func, ox, oy, fadeOut, "inQuad", "outQuad", 0.35, 0.1)
end

function DefaultTransition:draw()
    love.graphics.setColor(0,0,0,self.value)
    love.graphics.rectangle("fill", 0, 0, 424, 240)
    utils.graphics.resetColor()
end

return DefaultTransition