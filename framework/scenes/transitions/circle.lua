local TransitionParent = require "framework.scenes.transitions.canvas"
local DefaultTransition = TransitionParent:extend()

function DefaultTransition:new(func, ox, oy, fadeOut)
    DefaultTransition.super.new(self, func, ox, oy, fadeOut, "inQuad", "outQuad", 0.8, 0.1)
end

function DefaultTransition:drawCanvas()
    love.graphics.setColor(0,0,0,1)
    love.graphics.rectangle("fill", 0, 0, 424, 240)
    utils.graphics.resetColor()
    local value = 1 - self.value
    love.graphics.circle("fill",self.ox, self.oy, (424/2) * 1.5 * value)
end

return DefaultTransition