-- assets/animator :: the animator object. The animator object handle what
-- frame a sprite should draw.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Animator = Object:extend()

-- INIT FUNCTIONS
-- Initilizing and configuring option

function Animator:new(name)
    self.name = name
  self.sprite     = assets.sprites:get(name)
  self.frame      = 0
  self.frameTimer = 0
  self.currentAnimation = ""

  self.customSpeed = 0
  self.speedFactor = 1

  self:changeToDefaultAnimation()
end

function Animator:setCustomSpeed(customSpeed)
  self.customSpeed = customSpeed or 0
end

function Animator:setSpeedFactor(speedFactor)
  self.speedFactor = speedFactor or 1
end

-- UPDATE FUNCTIONS
-- Update the animation of the animator

function Animator:update(dt)
  if (self.currentAnimation == "") then
    core.debug:warning("animator", "no current animation data")
    return 0
  end

  local speed = self.sprite:getAnimationSpeed(self.currentAnimation, self.customSpeed)
  self.frameTimer = self.frameTimer + (speed * dt * self.speedFactor)

  if self.frameTimer > 1 then
    self.frameTimer = 0

    if (self.sprite:isAnimationOver(self.currentAnimation, self.frame)) then
        self.frame = self.sprite:getAnimationRestartFrame(self.currentAnimation)
        self:sendCallback()
    else
        self.frame = self.frame + 1
        self:sendFrameSignal()
    end
  end
end

-- ANIMATION HANDLING FUNCTIONS
-- Change the animation of the animator

function Animator:changeAnimation(name, restart)
  -- Force restart if animation name is different
  if (self.currentAnimation ~= name) then
    restart = true
  else
    restart = restart or false
  end

  self.currentAnimation   = name

  if (restart == true) then
    self.frame      = 0
    self.frameTimer = 0
  end
end

function Animator:changeToDefaultAnimation(restart)
  self:changeAnimation(self.sprite:getDefaultAnimation(), restart)
end

-- INFO FUNCTIONS
-- get information with these functions

function Animator:getCurrentAnimation()
  return self.currentAnimation
end

function Animator:getAnimationDuration(animation)
  return self.sprite:getAnimationDuration(animation)
end

function Animator:getAbsoluteFrame()
  return self.sprite:getAbsoluteFrame()
end

function Animator:animationExist(name)
    return self.sprite:animationExist(name)
end

function Animator:getDimensions()
  return self.sprite:getDimensions()
end

function Animator:getAreas()
  return self.sprite:getAreas(self.currentAnimation, self.frame)
end

-- CALLBACK FUNCTIONS
-- Handle getting a calback from the animation system

function Animator:setCallbackTarget(actor)
  self.callbackTarget = actor
end

function Animator:setCallback(actor)
  core.debug:warning("Animator", "Animator:setCallback is deprecated, prefer Animator:setCallbackTarget instead")
  self:setCallbackTarget(actor)
end

function Animator:sendCallback()
  if (self.callbackTarget ~= nil) then
    self.callbackTarget:animationEnded(self.currentAnimation)
  end
end

function Animator:sendFrameSignal()
    local signals = self.sprite:getFrameSignal(self.currentAnimation, self.frame)

    if (signals ~= nil and #signals > 0) then
        for _, signal in ipairs(signals) do
            self:trySendSignal(signal)
        end
    end
end

function Animator:trySendSignal(frame, signal)
  if (self.callbackTarget ~= nil) and (self.callbackTarget.receiveFrameSignal ~= nil) then
    self.callbackTarget:receiveFrameSignal(signal)
  end
end

-- DRAW FUNCTIONS
-- Draw animations using these functions

function Animator:draw(x, y, r, sx, sy, ox, oy, kx, ky)
  self.sprite:draw(self.currentAnimation, self.frame, x, y, r, sx, sy, ox, oy, kx, ky)
end

return Animator
