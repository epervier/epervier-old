-- scenes/animators :: the animator manager. Create on demand animators and can clone
-- them

--[[
  Copyright © 2024 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local AnimatorManager = Object:extend()
local Animator = require "framework.scenes.animators.animator"

function AnimatorManager:new()
    self.list = {}
end

function AnimatorManager:update(dt)
    for _, animator in pairs(self.list) do
        animator:update(dt)
    end
end

function AnimatorManager:init(name)
    if (self.list[name] == nil) then
        self.list[name] = Animator(name)
    end
end

function AnimatorManager:get(name)
    self:init(name)
    return self.list[name]
end

function AnimatorManager:clone(name)
    return Animator(name)
end

function AnimatorManager:draw(name, x, y, r, sx, sy, ox, oy, kx, ky)
    local animator = self:get(name)
    animator:draw(x, y, r, sx, sy, ox, oy, kx, ky)
end

return AnimatorManager