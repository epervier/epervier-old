-- modules/gui :: a simple gui system, that manage all overhead elements of the game.

--[[
  Copyright © 2021 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Gui = Object:extend()

local ElementList = require "framework.scenes.gui.mixins.elements"
local ScreenList = require "framework.scenes.gui.mixins.screens"
Gui:implement(ScreenList)
Gui:implement(ElementList)

local TransformDataStruct = require "framework.structures.tween"

function Gui:new(scene)
    self.scene = scene
    self:reset()
    self.sfx = {}
end

function Gui:reset()
    self:initElements()
    self:initScreens()
end

function Gui:update(dt)
    for _, element in pairs(self.elements) do
        if (element ~= nil) then
            element:updateElement(dt)
        end
    end
    for _, screen in pairs(self.screens) do
        if (screen ~= nil) then
            screen:update(dt)
        end
    end
end

-- TWEEN FUNCTIONS
-- Handle tweening

function Gui:transform(data, delay)
    delay = delay or 0

    local time = 0
    for _, rawTransform in ipairs(data) do
        local newTime = self:transformOne(rawTransform, delay)
        time = math.max(time, newTime)
    end
    return time
end

function Gui:playScreenTransform(screenname, name, delay)
    return self.screens[screenname]:playTransform(name, delay)
end

function Gui:transformOne(rawTransform, delay)
    delay = delay or 0

    local struct = TransformDataStruct[rawTransform[2]]
    assert(struct ~= nil, "Structure " .. rawTransform[1] .. " doesn't exists ")
    local transform = utils.table.parse(rawTransform, struct, 0)

    local time = transform.start + delay

    if transform.type == "tween" then
        self:newTween(transform.name, time, transform.duration, transform.target, transform.easing)
    elseif transform.type == "movement" then
        self:newMovement(transform.name, time, transform.duration, transform.x, transform.y, transform.easing)
    elseif transform.type == "switch" then
        self:newSwitch(transform.name, time, transform.bools)
    elseif transform.type == "delayFocus" then
        self:delayFocus(transform.name, time)
    end

    if (transform.duration ~= nil) then
        time = time + transform.duration
    end
    return time
end

function Gui:newTween(element, start, duration, target, easing)
    assert(self.elements[element] ~= nil, element .. " does not exists")
    self.elements[element]:newTween(start, duration, target, easing)
end

function Gui:newMovement(element, start, duration, x, y, easing)
    assert(self.elements[element] ~= nil, element .. " does not exists")
    self.elements[element]:newMovement(start, duration, x, y, easing)
end

function Gui:newSwitch(element, start, bools)
    assert(self.elements[element] ~= nil, element .. " does not exists")
    self.elements[element]:newSwitch(start, bools)
end

function Gui:delayFocus(element, start)
    assert(self.elements[element] ~= nil, element .. " does not exists")
    self.elements[element]:delayFocus(start)
end

function Gui:hideScreen(screenname)
    self.screens[screenname]:hide()
end


function Gui:showScreen(screenname, focusElement, widgetId, page, arbitraryDatas)
    self.screens[screenname]:show(focusElement, widgetId, page)
    self.screens[screenname]:setDatas(arbitraryDatas)
end

-- SOUND FUNCTIONS
-- Handle SFX

function Gui:addSFX(guiName, sfxName)
    self.sfx[guiName] = sfxName
end

function Gui:playSFX(sfxType)
    local sfxName = self.sfx[sfxType]
    if (sfxName ~= nil) then
        assets:playSFX(self.sfx)
    end
end

-- KEYBOARD FUNCTIONS
-- Handle keyboard

function Gui:keycheck(keys)
    local haveFocus = self:haveFocus()
    if (haveFocus) then
        local elem = self:getFocusedElement()
        for key,_ in pairs(keys) do
            if (keys[key].isPressed and not elem:isTransforming()) then
                elem:keypressed(key)
            end
        end
    end
    return haveFocus
end

-- DRAW FUNCTIONS
-- Draw the menu and its content

function Gui:redraw()
    for _, element in pairs(self:getVisibleElement(true)) do
        element:redraw()
    end
    for _, element in pairs(self:getVisibleElement(false)) do
        element:redraw()
    end
end

function Gui:drawTop()
    for _, element in ipairs(self:getVisibleElement(true)) do
        element:drawElement()
    end
end

function Gui:drawBottom()
    for _, element in ipairs(self:getVisibleElement(false)) do
        element:drawElement()
    end
end

return Gui