local MenuModel = Object:extend()
local Page = require "framework.scenes.gui.menus.model.page"

local function updateWidgetByOrder(a, b)
    if a.order ~= b.order then
        return a.order < b.order
    else
        return a.creationID < b.creationID
    end
end

-- INIT FUNCTIONS
-- Initialize and basic functions.

function MenuModel:new()
    self:clear()

    self.list = {}
    self.selected = 0
    self.selectedPrevious = 0
    self.cancel = 0
    self.limit = -1
    -- self:updateWidgetSize()
    self.hoverFunc = nil
end

function MenuModel:clear()
    self.pages = {}
    self:addPage("main")
end

-- PAGE FUNCTIONS
-- Handle pages

function MenuModel:addPage(pageName)
    local page = Page()
    page.name = pageName
    self.pages[pageName] = page
    self.currentPage = pageName
    return page
end

function MenuModel:addSubmenu(pageName, parent)
    local page = self:addPage(pageName)
    page:setParent(parent)
end

function MenuModel:removePage(pageName)
    self.pages[pageName] = nil
end

function MenuModel:pageExists(pageName)
    return (self.pages[pageName] ~= nil)
end

function MenuModel:getPage(pageName)
    return self.pages[pageName]
end

function MenuModel:switch(pageName)
    if (self:pageExists(pageName)) then
        self.currentPage = pageName
        self:hoverAction()
    end
end

function MenuModel:back()
    local page = self:getCurrentPage()
    self:switch(page:getParent())
end

function MenuModel:getCurrentPage()
    return self:getPage(self.currentPage)
end

function MenuModel:getCurrentPageName()
    return self.currentPage
end

-- UPDATE/DRAW FUNCTIONS
-- All the update functions

function MenuModel:update(dt)
    local page = self:getCurrentPage()
    page:update(dt)
end

function MenuModel:updateWidgetsOrder()
    local page = self:getCurrentPage()
    page:updateWidgetByOrder()
end

function MenuModel:updateWidgetsID()
    local page = self:getCurrentPage()
    page:updateWidgetsID()
end

-- ACTION FUNCTIONS
-- All the actions callback used by the widgets

function MenuModel:addHoverAction(func)
    self.hoverFunc = func
end

function MenuModel:cancelAction()
    local page = self:getCurrentPage()
    page:cancelAction()
end

function MenuModel:action(id, type)
    local page = self:getCurrentPage()
    page:action(id, type)
end

function MenuModel:selectedAction()
    local page = self:getCurrentPage()
    page:selectedAction()
end

function MenuModel:hoverAction()
    local page = self:getCurrentPage()
    if (self.hoverFunc ~= nil) then
        page:hoverAction(self.hoverFunc)
    end
end

function MenuModel:lateralAction(func, key)
    local page = self:getCurrentPage()
    page:lateralAction(func, key)
end


-- WIDGET FUNCTIONS
-- All the functions to handle widgets

function MenuModel:addWidget(newWidget)
    local page = self:getCurrentPage()
    page:addWidget(newWidget)
end

function MenuModel:getList(first, lenght)
    local page = self:getCurrentPage()
    return page:getList(first, lenght)
end

function MenuModel:removeDestroyedWidgets()
    local page = self:getCurrentPage()
    page:removeDestroyedWidgets()
end

function MenuModel:lenght()
    local page = self:getCurrentPage()
    return page:lenght()
end

function MenuModel:widgetExist(id)
    local page = self:getCurrentPage()
    return page:widgetExist(id)
end

function MenuModel:setLimit(limit)
    local page = self:getCurrentPage()
    page:setLimit(limit)
end

-- CANCEL FUNCTIONS
-- Add a widget as a "cancel" function 

function MenuModel:setCancelWidget(id)
    local page = self:getCurrentPage()
    page:setCancelWidget(id)
end

function MenuModel:getCancelWidget()
    local page = self:getCurrentPage()
    return page:getCancelWidget()
end

-- CURSOR FUNCTIONS
-- Set or move the cursor of the menu

function MenuModel:getSelected()
    local page = self:getCurrentPage()
    return page:getSelected()
end

function MenuModel:haveCursor()
    local page = self:getCurrentPage()
    return page:haveCursor()
end

function MenuModel:trySelectWidget(cursorid)
    local page = self:getCurrentPage()
    local isSuccess = page:trySelectWidget(cursorid)
    if (isSuccess) then
        self:hoverAction()
    end
    return isSuccess
end

function MenuModel:setCursor(cursorid)
    local page = self:getCurrentPage()
    page:setCursor(cursorid)
    self:hoverAction()
end

function MenuModel:moveCursorAbsolute(newSelected)
    local page = self:getCurrentPage()
    page:moveCursorAbsolute(newSelected)
    self:hoverAction()
end

function MenuModel:moveCursor(relative)
    local page = self:getCurrentPage()
    page:moveCursor(relative)
    self:hoverAction()
end

-- DRAW
-- Draw widget
function MenuModel:redraw()
    local page = self:getCurrentPage()
    page:redraw()
end


return MenuModel
