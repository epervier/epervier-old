-- listbox : add a vertical list of widgets.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local cwd  = (...):gsub('%.listbox$', '') .. "."
local Menu = require(cwd .. "parent")

local ListBox = Menu:extend()

local View1D = require(cwd .. "views.view1D")

-- INIT FUNCTIONS
-- Initialize and configure functions.

function ListBox:new(name, x, y, w, h, slotNumber)
  self.view = View1D(slotNumber)
  ListBox.super.new(self, name, x, y, w, h)
  self.h = slotNumber * self.widgetSize.h -- On fait en sorte que la hauteur
  -- soit un multiple du nombre de slot et de leur hauteur
  self.lateralFunc = nil
  self.packAtEnd = false
end

-- UPDATE FUNCTIONS
-- Update the menu every step.

function ListBox:updateWidgetSize()
  self.widgetSize.h = math.floor( self.h / self.view.slotNumber )
  self.widgetSize.w = self.w
end

function ListBox:update(dt)
  self.view:updateFirstSlot(self.widget:getSelected())
end

function ListBox:resetView()
  self.view:reset()
end

function ListBox:addLateralAction(func)
  self.lateralFunc = func
end

-- KEYBOARD FUNCTIONS
-- Handle input from keyboard/controllers.

function ListBox:moveByKeys(key)
  if key == 'up' then
    self.widget:moveCursor(-1)
    self:playNavigationSound()
    self.canvas.needRedraw = true
  end

  if key == 'down' then
    self.widget:moveCursor(1)
    self:playNavigationSound()
    self.canvas.needRedraw = true
  end

  if (self.lateralFunc ~= nil and (key == 'left' or key == 'right')) then
    self.widget:lateralAction(self.lateralFunc, key)
  end
end

-- POSITION FUNCTIONS
-- Get a widget by a position.

function ListBox:getWidgetAtPoint(x, y)
  return (self.view.firstSlot + math.floor(y / self.widgetSize.h))
end

-- DRAW FUNCTIONS
-- draw the menu and the rest of content.

function ListBox:getListPart(relativeNumber)
  if (self.packAtEnd) then relativeNumber = relativeNumber + math.max(0, self.view.slotNumber - self.widget:lenght()) end
  return 0, (relativeNumber) * self.widgetSize.h, self.w, self.widgetSize.h
end

function ListBox:drawTexture()
  self.view:updateFirstSlot(self.widget:getSelected())
  local listWidget = self.widget:getList(self.view.firstSlot, self.view.slotNumber)

  for i, widget in ipairs(listWidget) do
    local x, y, w, h = self:getListPart(i - 1)
    self:drawWidgetBackground(x + self.canvas.padding, y + self.canvas.padding, w, h)
    widget:drawWidget(x + self.canvas.padding, y + self.canvas.padding, w, h)
  end
end

function ListBox:getGraphicalCursorPosition()
  local x, y, w, h = self:getListPart(self.widget:getSelected() - self.view.firstSlot)

  return self:getListPart(self.widget:getSelected() - self.view.firstSlot)
end

return ListBox
