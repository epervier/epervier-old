-- view1D.lua : A basic 1D view for menus.
-- Must be used as a subobject of a Menu

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local View1D = Object:extend()

function View1D:new(slotNumber)
    self.slotNumber  = slotNumber
    self.firstSlot   = 1
end

function View1D:reset()
    self.firstSlot   = 1
end

function View1D:updateFirstSlot(widgetID)
    self.firstSlot = math.max(1, widgetID - (self.slotNumber  - 1), math.min(widgetID, self.firstSlot))
end

function View1D:isBeforeView(x)
    return (x < self.firstSlot)
end

function View1D:isAfterView(x)
    return (x >= (self.firstSlot + self.slotNumber))
end

return View1D