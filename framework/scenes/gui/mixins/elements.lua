local ElementList = Object:extend()

function ElementList:initElements()
    self.elements = {}
    self.focusedElement = nil
    self.lastFocused = nil
    self.nbrElement = 0
end

function ElementList:addElement(name, element)
    self.nbrElement = self.nbrElement + 1
    self.elements[name] = element
    return self.nbrElement
end

function ElementList:deleteElement(name)
    self.elements[name] = nil
end

function ElementList:setFocus(name, widgetId, page)
    assert(self:elementExists(name), "Element " .. name .. " doesn't exists")
    self:storeLastFocus()
    self.focusedElement = name
    self.elements[name].isVisible = true
    if (widgetId ~= nil) then
        self.elements[name]:setSubFocus(widgetId, page)
    end
end

function ElementList:removeFocus()
    self:storeLastFocus()
    self.focusedElement = nil
end

function ElementList:storeLastFocus()
    if (self.focusedElement ~= nil) then
        self.lastFocused = self.focusedElement
    end
end

function ElementList:setLastFocus()
    if (self:elementExists(self.lastFocused)) then
        self:setFocus(self.lastFocused)
    end
end

function ElementList:elementExists(name)
    return (self:getElement(name) ~= nil)
end

function ElementList:haveFocus()
    return self:elementIsVisible(self.focusedElement)
end

function ElementList:getFocusedElement()
    return self:getElement(self.focusedElement)
end

function ElementList:getElement(name)
    if (not utils.string.isEmpty(name)) then
        return self.elements[name]
    end
    return nil
end

function ElementList:getVisibleElement(topLayer)
    local visibleList = {}
    for _, element in pairs(self.elements) do
        if (element ~= nil) then
            if (element:getVisibility() and ((element.depth) < 0 == topLayer)) then
                table.insert(visibleList, element)
            end
        end
    end

    table.sort(visibleList, function (a, b)
        if (a.depth == b.depth) then
            return (a.creationId < b.creationId)
        else
            return (a.depth > b.depth)
        end
    end)

    return visibleList
end

function ElementList:elementIsVisible(name)
    if (self:elementExists(name)) then
        return self.elements[name]:getVisibility()
    end
    return false
end

return ElementList