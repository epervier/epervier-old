local ScreenList = Object:extend()

function ScreenList:initScreens()
    self.screens = {}
end

function ScreenList:addScreen(name, screen)
    self.screens[name] = screen
end

function ScreenList:deleteScreen(name)
    self.screens[name]:purgeElements()
    self.screens[name] = nil
end

function ScreenList:getScreen(name)
    return self.screens[name]
end

return ScreenList