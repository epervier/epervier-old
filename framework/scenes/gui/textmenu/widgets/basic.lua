-- widget : the base widget for a textmenu
-- It replace the functions to get the font and the selectionColor by wrapper to
-- TextMenu.

--[[
  Copyright © 2020 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local TextWidget = require "framework.scenes.gui.menus.widgets.text"

local TextMenuWidget = TextWidget:extend()

function TextMenuWidget:new(menuName, label, position)
    local position = position or "left"
    TextMenuWidget.super.new(self, menuName, "medium", label, position, 0)
end

function TextMenuWidget:getFont()
    return self.menu:getFont()
end

function TextMenuWidget:getSelectedColor()
    local selectedColor = self.menu:getSelectedColor()
    if (selectedColor ~= nil) then
        return selectedColor.r, selectedColor.g, selectedColor.b
    else
        return self:getColor()
    end
end

function TextMenuWidget:getPadding()
    return self.menu:getPadding()
end

function TextWidget:getPaddingLeft()
    return self.menu:getPaddingLeft() or self:getPadding()
end

function TextWidget:getPaddingRight()
    return self.menu:getPaddingRight() or self:getPadding()
end

return TextMenuWidget