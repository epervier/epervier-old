local GuiScreen = Object:extend()
local ElementList = require "framework.scenes.gui.mixins.elements"
GuiScreen:implement(ElementList)

local TweenManager = require "framework.classes.time"
local ScreenSet = require "framework.scenes.gui.screen.screenset"

local elementDataStruct = require "framework.structures.elementData"

function GuiScreen:new(name)
    self:initWrapper()
    self.name = name
    self.isVisible = false
    self.transforms = {}
    self.tweens = TweenManager(self)

    self:reset()
    self:registerElements()
    self.gui:addScreen(name, self)

    self.defaultFocus = nil
end

function GuiScreen:initWrapper()
    local scene = core.scenemanager.nextScene or core.scenemanager:getCurrent()
    self.scene = scene
    self.gui = scene.gui
    -- Présent pour la compatibilité
    self.controller = self.gui
end

function GuiScreen:update(dt)
    self.tweens:update(dt)
end

function GuiScreen:show(focusElement, widgetId, page)
    self:showSimple(focusElement, widgetId, page)
    if (self.set ~= nil) then
        self.set.owner:show()
    end
end

function GuiScreen:showSimple(focusElement, widgetId, page)
    focusElement = focusElement or self.defaultFocus
    if (not self.isVisible) then
        self.isVisible = true
        local delay = 0
        if (self.set ~= nil) then
            delay = self.set:setCurrentScreen(self.name)
        end

        if (self.transforms["show"] ~= nil) then
            if (delay == 0) then
                self:showWithSubScreen(focusElement, widgetId, page)
            else
                self.tweens:newFunc(delay, "focus", function () self:showWithSubScreen(focusElement, widgetId, page) end)
            end
        end
    end
end

function GuiScreen:showWithSubScreen(focusElement, widgetId, page)
    self:playTransform("show")
    if (focusElement ~= nil) then
        self.gui:setFocus(focusElement, widgetId, page)
    end

    if (self.subscreens ~= nil) then
        self.subscreens:show()
    end
end

function GuiScreen:setDatas(datas)

end

function GuiScreen:hide()
    local time = 0
    if (self.isVisible) then
        if (self.transforms["hide"] ~= nil) then
            time = self:playTransform("hide")
            self.tweens:newFunc(time, "hide", function ()
                self.isVisible = false
            end)
        end

        if (self.subscreens ~= nil) then
            self.subscreens:hideCurrent()
        end
    end
    return time
end

function GuiScreen:addTransform(name, transform)
    self.transforms[name] = transform
end

function GuiScreen:playTransform(name, delay)
    return self.gui:transform(self.transforms[name], delay)
end

function GuiScreen:reset()
    self:initElements()
end

function GuiScreen:registerElements()
    local elementList = self:createElements()
    for _, rawElement in ipairs(elementList) do
        if (rawElement.is ~= nil) then
            self:addElement(rawElement.name, rawElement)
            rawElement.screen = self
        else
            local elemData = utils.table.parse(rawElement, elementDataStruct, 3)
            local element = elemData.element
            self:addElement(element.name, element)
            if (elemData.focus == true) then
                element:getFocus()
            end
            if (elemData.delay > 0) then
                element.isVisible = false
                element:newSwitch(elemData.delay, {"isVisible"})
            end
            if (elemData.depth ~= nil) then
                element.depth = elemData.depth
            end
            if (elemData.keypress ~= nil) then
                element:setKeyPressAction(elemData.keypress)
            end
            element.screen = self
        end
    end
end

function GuiScreen:createElements()
    -- Empty function
end

function GuiScreen:setParentSet(set)
    self.set = set
end

function GuiScreen:addSubscreen(screen)
    self:initSubscreen()
    self.subscreens:add(screen)
end

function GuiScreen:showSubscreen(screenname)
    if (self.subscreens ~= nil) then
        self.subscreens:show(screenname)
    end
end

function GuiScreen:initSubscreen()
    if (self.subscreens == nil) then
        self.subscreens = ScreenSet(self)
    end
end

return GuiScreen