local Parent = require "framework.scenes.gui.elements.parent"
local CompositeElement = Parent:extend()

function CompositeElement:new(name, x, y, childrenList)
    self.children = {}
    self:addChildren(childrenList)
    CompositeElement.super.new(self, name, x, y, 1, 1)
    self.isVisible = true
end

function CompositeElement:addChildren(list)
    for _, childData in ipairs(list) do
        self:addChild(childData[1], childData[2], childData[3])
    end
end

function CompositeElement:addChild(children, relx, rely)
    local child = {}
    child.name = children.name
    child.relx = relx
    child.rely = rely
    table.insert(self.children, child)
end

function CompositeElement:update(dt)
    for _, child in ipairs(self.children) do
        local childElement = self.gui.elements[child.name]
        childElement.x = self.x + child.relx
        childElement.y = self.y + child.rely
        childElement.isVisible = self.isVisible
        childElement.depth = self.depth
    end
end

return CompositeElement