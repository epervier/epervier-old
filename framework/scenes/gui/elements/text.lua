local Parent = require "framework.scenes.gui.elements.parent"
local TextElement = Parent:extend()

function TextElement:new(name, fontName, text, x, y, align)
    TextElement.super.new(self, name, x, y, 1, 1)
    self.text = text
    self.fontName = fontName

    self.align = align
    self.opacity = 1
end

function TextElement:getText()
    return self.text
end

function TextElement:draw()
    self.font:setColor(1, 1, 1, self.opacity)
    assets.draw()
    assets:printf(self.fontName, self:getText(), self.x, self.y, -1, self.align)
    love.graphics.setColor(1, 1, 1, 1)
end

return TextElement