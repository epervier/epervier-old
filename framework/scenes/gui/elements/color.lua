local Parent = require "framework.scenes.gui.elements.parent"
local ColorElement = Parent:extend()

function ColorElement:new(name, r, g, b, opacity)
    local w, h = core.screen:getDimensions()
    ColorElement.super.new(self, name, self.x, self.y, w, h)
    self.r = r or 1
    self.g = g or 1
    self.b = b or 1
    self.opacity = opacity or 1
end

function ColorElement:draw()
    love.graphics.setColor(self.r, self.g, self.b, self.opacity)
    love.graphics.rectangle("fill", 0, 0, self.w, self.h)
    utils.graphics.resetColor()
end

return ColorElement