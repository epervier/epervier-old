local Parent = require "framework.scenes.gui.elements.variable"
local CounterElement = Parent:extend()

function CounterElement:new(name, fontName, object, varName, nbrs, x, y, align)
    CounterElement.super.new(self, name, fontName, object, varName, x, y, align)
    self.nbrs = nbrs or 0
end

function CounterElement:getText()
    return utils.math.numberToString(CounterElement.super.getText(self), self.nbrs)
end

return CounterElement