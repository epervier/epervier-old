local Parent = require "framework.scenes.gui.elements.drawable"
local TileElement = Parent:extend()

function TileElement:new(name, assetName, id, x, y,r,sx,sy,ox,oy, opacity)
    self:initWrapper()
    local asset = assets.tileset:get(assetName) 
    assert(asset ~= nil, assetName .. " ( tileset ) doesn't exist")
    self.tileId = id

    TileElement.super.new(self, name, asset, x, y,r,sx,sy,ox,oy, opacity)
end

function TileElement:draw()
    love.graphics.setColor(1, 1, 1, self.opacity)
    self.drawable:drawTile(self.tileId, self.x,self.y,self.r,self.sx,self.sy,self.ox,self.oy)
    utils.graphics.resetColor()
end

return TileElement