-- scenes.lua :: the scene object, that aim to give a better control to the engine
-- to the different scene, without having to call too much boilerplate

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Scene  = Object:extend()

local Gui     = require "framework.scenes.gui"

local AnimatorManager = require "framework.scenes.animators"

-- INIT FUNCTIONS
-- Initialize and configure the scene

function Scene:new()
  self.mouse = {}
  self.mouse.x, self.mouse.y = core.screen:getMousePosition()

  self.animators  = AnimatorManager()
  self.sources    = core.input:getSources()
  self.gui = Gui(self)

  self.inputLocked      = true
  self.inputLockedTimer = 2

  self.dialog = nil

  self:initWorld()

  self:register()
end

function Scene:register()
  core.scenemanager:setScene(self)
end

function Scene:clear()
  -- TODO: send automatic cleanups to the different elements of the scene
end

function Scene:restored()

end

-- UPDATE FUNCTIONS
-- Handle stuff that happens every steps

function Scene:updateScene(dt)
  self:updateStart(dt)
  self:setKeys()
  self.animators:update(dt)
  self:updateGUI(dt)
  self:updateWorld(dt)
  self:update(dt)
  self:updateEnd(dt)
end

function Scene:updateStart(dt)

end

function Scene:update(dt)
  -- Empty function, is just here to avoid crash
end

function Scene:updateEnd(dt)

end

function Scene:updateWorld(dt)
  if ((self.world ~= nil) and (self.world.isActive) and (self.dialog == nil)) then
    self.world:update(dt)
  end
end

function Scene:updateGUI(dt)
  if (self.gui ~= nil) then
    self.gui:update(dt)
    if (core.screen:isActive()) then
      self.gui:keycheck(self:getKeys(1))
    end
  end
end

-- MOUSE FUNCTIONS
-- Make the scene support the pointer

function Scene:mousemoved(x, y, dx, dy)
  -- Empty function, is just here to avoid crash
end

function Scene:mousepressed( x, y, button, istouch )
  -- Empty function, is just here to avoid crash
end

-- WORLD FUNCTIONS
-- Basic functions to manage the world

function Scene:initWorld()
  self.world = nil
end

function Scene:registerWorld(world)
  self.world = world
end

-- KEYBOARD FUNCTIONS
-- Add send keys functions to the scene

function Scene:keypressed( key, scancode, isrepeat )

end

function Scene:keyreleased( key )

end

-- DRAW FUNCTIONS
-- Draw the scene and its content

function Scene:redraw()
  self.gui:redraw()
end

function Scene:drawScene()
  self:drawStart()
  self:drawWorld()
  self:draw()
  self.gui:drawBottom()
  self:drawEnd()
  core.screen:drawTransition()
  self.gui:drawTop()
  self:drawOverTransition()
end

function Scene:drawStart()

end

function Scene:draw()

end

function Scene:drawEnd()

end

function Scene:drawOverTransition()

end

function Scene:drawWorld(dt)
  if (self.world ~= nil) then
    self.world:draw()
  end
end

-- INPUT FUNCTIONS
-- Handle inputs from keyboard/controllers

function Scene:setKeys()
  if (self.inputLocked) then
    self.inputLockedTimer = self.inputLockedTimer - 1
    if (self.inputLockedTimer <= 0 ) then
      self.inputLocked = false
    end
  else
    self.sources = core.input.sources
  end
end

function Scene:getKeys(sourceid)
  if sourceid == nil then
    core.debug:warning("scene", "no sourceid detected, will default to 1")
  end

  local sourceid = sourceid or 1

  if (self.inputLocked) then
    core.debug:print("scene", "inputs are currently locked")
    return core.input.fakekeys
  else
    return self.sources[sourceid].keys
  end
end

function Scene:flushKeys()
  core.input:flushKeys()
  self.sources = core.input:getSources()
  self.inputLockedTimer = 2
  self.inputLocked      = true
end

return Scene
