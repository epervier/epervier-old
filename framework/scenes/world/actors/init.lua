-- world/actors :: the base actor object, that handle everything important an
-- actor can do in a world.

--[[
  Copyright © 2024 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Actor = Object:extend()
local Physics = require "framework.scenes.world.actors.physics"
local Visuals = require "framework.scenes.world.actors.visuals"
Actor:implement(Physics)
Actor:implement(Visuals)

-- Managers
local TweenManager = require "framework.classes.time"

function Actor:new(world, position)
  assert(self.def ~= nil, "Actor should be created with the actor() function and a definition")
  self.world = world
  self.type = self.def.type
  self:initPhysics(position, self.def.dimensions, self.def.isSolid)
  self:_initVisual(self.def.visuals or {})

  self.isDestroyed = false
  self.depth = self.def.depth or 0

  self.timers = TweenManager(self)

  self:onInit()
end

function Actor:getShape()
  return self:_getShapeData()
end

function Actor:getViewCenter()
  return self:_getViewCenter()
end

function Actor:updateActor(dt)
  self.timers:update(dt)
  self:update(dt)
  self:_updateVisual(dt)
  self:applyMovement(dt)
end

function Actor:destroy()
  self.world:removeActor(self)
  self:_removeBody()
  self:_removeVisual()
  self.isDestroyed = true
end

function Actor:draw()
  self:drawShadow()
  self:_drawVisual()
  if (self.def.drawHitboxes == true) then
    self:_drawHitboxes()
  end
end

function Actor:drawShadow()
  if (self.world.type ~= "3D" and self.world.drawShadow ~= true) then
    return
  end
  if (self.zGround == nil) then
    return
  end

  local r, g, b, a = love.graphics.getColor( )
  local x, y = self.position.x + (self.dimensions.w / 2), self.position.y + (3 * self.dimensions.h / 4) - self.zGround
  love.graphics.setColor(0, 0, 0, .3)
  love.graphics.ellipse( "fill", math.floor(x), math.floor(y), self.dimensions.w / 2, self.dimensions.w / 4 )
  love.graphics.setColor(r, g, b, a)
end

-- Callbacks
function Actor:onInit()

end

function Actor:update(dt)

end

function Actor:timerResponse(name)
  -- Placeholder
end

function Actor:hitboxResponse(name, type, collision)
  -- Placeholder
end

function Actor:animationEnded(animation)
  -- Placeholder
end

function Actor:collisionResponse(collision)
    -- Placeholder
end

function Actor:drawHUD()

end

return Actor
