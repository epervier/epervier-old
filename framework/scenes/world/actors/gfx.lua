-- gfx.lua :: a basic 2D GFX.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local GFX = extend("framework.scenes.world.actors")

local physics = require "framework.scenes.world.actors.physics.utils"

function GFX:new(world, position, spritename, is3D, useMiddle)
  local width, height = world.animators:get(spritename):getDimensions()

  local dimensions
  if (is3D) then
    dimensions = {w = width, h = width, d = height}
  else
    dimensions = {w = width, h = height, d = 1}
  end

  -- !HACK!
  -- Cheat by creating at runtime the definition
  self.def = {
    type = "gfx",
    dimensions = dimensions,
    isSolid = false,
    visuals = {
        mode = "sprite",
        assetName = spritename,
        clone = true,
        withCallbacks = true
    }
  }

  if (useMiddle) then
    local dimVector = physics.dimensionsToVector(dimensions)
    position = position + (dimVector / 2)
  end

  GFX.super.new(self, world, position)
end

function GFX:animationEnded(animation)
  core.debug:print("gfx", 'Current animation "' .. animation .. '" have ended, destroying gfx')
  self:destroy()
end

return GFX
