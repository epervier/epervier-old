-- physics/utils :: physics utilities, used to make some computation
-- self-contained

--[[
  Copyright © 2024 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local physics = {}
local Vector3D = require "framework.libs.brinevector3D"
local tz = utils.math.toZero

local function _norm(spd, nor, bfac)
    -- Si la vitesse et la normal ont des valeurs différente (donc la vitesse amène à une collision "solide")
    -- Alors, on inverse la vitesse et la relie au facteur
    -- de rebond (par exemple si bfac = 0, cela veut dire pas de 
    -- rebonrd, et l'objet s'arrête)
    -- La normal est généralement l'inverse de la vitesse en cas de collision
    if (nor < 0 and spd > 0) or (nor > 0 and spd < 0) then
        return -spd * bfac
    end
    return spd
end

local function _toOnPx(axis, grav)
    return axis + utils.math.sign(grav)
end

local function _checkGravOnAxis(norm, grav)
    if (grav ~= 0 and norm == -utils.math.sign(grav)) then
        return true
    end
    return false
end

function physics.applyFriction(v, f)
    return Vector3D(tz(v.x, f.x), tz(v.y, f.y), tz(v.z, f.z))
end

function physics.applyCollisionNormal(spd, nor, bfac)
    -- On applique le calcul de normale de collision sur tout les axes
    return Vector3D(_norm(spd.x, nor.x, bfac), _norm(spd.y, nor.y, bfac), _norm(spd.z, nor.z, bfac))
end

function physics.getGroundVector(ps, grv)
    return Vector3D(_toOnPx(ps.x, grv.x), _toOnPx(ps.y, grv.y), _toOnPx(ps.z, grv.z))
end

function physics.compareGravToNormal(norm, grav)
    return (_checkGravOnAxis(norm.x, grav.x) or _checkGravOnAxis(norm.y, grav.y) or _checkGravOnAxis(norm.z, grav.z))
end

function physics.dimensionsToVector(dimensions)
    return Vector3D(dimensions.w, dimensions.h, dimensions.d)
end

function physics.vectorToDimensions(vector)
    return {w = vector.x, h = vector.y, d = vector.z}
end

function physics.getAxis(axis, vector)
    if (axis == "x") then
        return vector.x
    elseif (axis == "y") then
        return vector.y
    elseif (axis == "z") then
        return vector.z
    else
        return nil
    end
end


return physics