-- actors/physics :: the basic physics system of the actor,
-- handle all hitboxes and collisions

--[[
  Copyright © 2024 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Physics = Object:extend()
local Vector3D = require "framework.libs.brinevector3D"
local Hitbox = require "framework.scenes.world.actors.physics.hitbox"

local physicsUtils = require "framework.scenes.world.actors.physics.utils"

-- INITIALISATION

function Physics:initPhysics(position, dimensions, isSolid)
    self.position = Vector3D(position.x, position.y, position.z or 0)
    self.speed = Vector3D(0, 0, 0)

    self:setGravity(self.def.gravity or {})
    self:setFriction(self.def.friction or {})
    self.bounceFactor = self.def.bounceFactor or 0

    self.isSolid = (self.def.isSolid == true)

    self.haveCollisions = true

    if (self.def.onPlayerCollision ~= nil) then
        self.onPlayerCollision = self.def.onPlayerCollision
    end

    -- les dimensions restent pour l'instant une table.
    self.dimensions = {w = dimensions.w, h = dimensions.h, d = dimensions.d or 1}

    -- Init the bump filter
    self.filter = function(item, other)
        if (other.owner == self) then
            -- ignore every collision with our own hitboxes
            return nil
        elseif (other.isSolid) then
            return "slide"
        else
            return "cross"
        end
    end

    self:_initHitboxes()
end

function Physics:_removeBody()
    if (self.haveCollisions) then
        self.mainHitbox:destroy()
        self:purgeHitbox()
    end
    self.haveCollisions = false
end

-- Setter functions

function Physics:setGravity(gravity)
    local axis, value = gravity.axis or "", gravity.value or 0
    self.gravity = Vector3D(0, 0, 0)
    self.gravityAxis = axis or ""
    if (self.gravityAxis == "") then
        return
    end
    if (axis == "x") then
        self.gravity.x = value
    elseif (axis == "y") then
        self.gravity.y = value
    elseif (axis == "z") then
        self.gravity.z = value
    else
        error("Axis " .. axis .. " not recognized")
    end
end

function Physics:setFriction(friction)
    self.friction = Vector3D(friction.x or 0, friction.y or 0, friction.z or 0)
end

-- Getter functions
-- Help to get informations from the physics

function Physics:getFuturePosition(dt)
    return self.position + (self.speed * dt)
end

function Physics:isAffectedByGravity()
    local spd = physicsUtils.getAxis(self.gravityAxis, self.speed)
    local grv = physicsUtils.getAxis(self.gravityAxis, self.gravity)

    if (spd == nil or grv == nil) then
        return false
    end

    return utils.math.sign(spd) == utils.math.sign(grv)
end

function Physics:packForHitbox()
    return {
        position = {x = 0, y = 0, z = 0},
        dimensions = self.dimensions
    }
end

-- Boucle d'application de mouvements
-- TODO: déterminer vraiment ce qui est interne ou non

function Physics:applyMovement(dt)
    -- todo: update hitboxes
    self.onGround = false

    self.speed = self.speed + (self.gravity * dt)
    if self:isAffectedByGravity() then
        self:checkGround()
    end

    local _, cols, colNumber = self:move(self:getFuturePosition(dt))

    self:solveAllCollisions(cols)

    self.speed = physicsUtils.applyFriction(self.speed, self.friction * dt)

    -- Les autres hitboxes traverses tout
    self:applyHitboxesCollisions(function(item, other) return "cross" end)

    if (self.visual ~= nil) then
        self:getGroundZ()
        self.world:updateShape(self)
    end
end

function Physics:move(position)
    local cols, colNumber = {}, 0
    if (self.isDestroyed == false) then
      self.position, cols, colNumber = self.mainHitbox:checkCollisionAtPoint(position, self.filter)
      self.mainHitbox:updatePosition()
    end

    return self.position, cols, colNumber
end

function Physics:checkCollisionAtPoint(position)
    local cols, colNumber = {}, 0
    if (self.isDestroyed == false) then
      _, cols, colNumber = self.mainHitbox:checkCollisionAtPoint(position, self.filter)
    end
    return self.position, cols, colNumber
end

function Physics:solveAllCollisions(cols)
    for i, col in ipairs(cols) do
        self:_applyCollisionResponses(col)
        if (col.type == "touch") or (col.type == "bounce") or (col.type == "slide") then
            --self:changeSpeedToCollisionNormal(col.normal)
            self.speed = physicsUtils.applyCollisionNormal(self.speed, col.normal, self.bounceFactor)
        end
    end
end

function Physics:checkGround()
    local dpos = physicsUtils.getGroundVector(self.position, self.gravity)
    local _, cols, colNumber = self:checkCollisionAtPoint(dpos)
    for i, col in ipairs(cols) do
        if (col.type == "touch") or (col.type == "bounce") or (col.type == "slide") then
            self.onGround = physicsUtils.compareGravToNormal(col.normal, self.gravity)
        end
    end
end

function Physics:getGroundZ()
    if (self.world.type ~= "3D") then
        return 0
    end
    -- On check to les acteurs qui pourraient être en dessous
    local bodiesBehind = self.world:getBodiesInCube(self.position.x, self.position.y, self.position.z - 10000, self.dimensions.w, self.dimensions.h, 10000)

    local z = nil
    for _, body in ipairs(bodiesBehind) do
        if (body.type ~= "main" and body.isSolid and body.owner ~= self) then
            local zToCheck = body.position.z + body.dimensions.d
            if (z == nil) then
                z = zToCheck
            else
                z = math.max(z, zToCheck)
            end
        end
    end
    if (z ~= nil) then
        self.zGround = math.floor(z)
    end
end

function Physics:_applyCollisionResponses(col)
    if (self.type == "player") and (col.other.owner ~= nil) and (col.other.owner.onPlayerCollision ~= nil) then
        col.other.owner:onPlayerCollision(self, "main", col)
    end
    self:collisionResponse(col)
end

-- HITBOXES HANDLING
-- Handle, create and use hitboxes

function Physics:_initHitboxes(hitboxObj)
    self:_initMainHitbox()

    self.hitboxes = {}
    self.hitboxFromSprite = true
end

function Physics:_initMainHitbox()
    self.mainHitbox = Hitbox(self, self.type, self:packForHitbox(), {x = 0, y = 0}, self.isSolid)
    self.mainHitbox:advertiseAsMainHitbox()
end

function Physics:setHitboxes(areas)
    self:purgeHitbox()
    for i, area in ipairs(areas) do
        if (area.type == "main") then
            self.mainHitbox:modify(area.box.position, area.box.dimensions)
        else
            self:addHitbox(area.type .. "" .. i, area.type, area.box, self.visual.scale, area.isSolid)
        end
    end
end

function Physics:addHitbox(name, type, data, scale, isSolid)
    if (self.hitboxes[name] ~= nil) then
        core.debug:logWarn("PhysicalActor", "the hitbox " .. name .. " already exists")
        return
    end

    self.hitboxes[name] = Hitbox(self, type, data, scale, (isSolid == true))
    return self.hitboxes[name]
end

function Physics:removeHitbox(name)
    if (self.hitboxes[name] ~= nil) then
        self.hitboxes[name]:destroy()
        self.hitboxes[name] = nil
    end
end

function Physics:purgeHitbox()
    for k, v in pairs(self.hitboxes) do
        v:destroy()
    end
    self.hitboxes = {}
end

-- Collisions

function Physics:applyHitboxesCollisions(filter)
    for k, v in pairs(self.hitboxes) do
        self:applyHitboxCollisions(k, filter)
    end
end

function Physics:applyHitboxCollisions(name, filter)
    local cols, colNumber = {}, 0
    local filter = filter or self.filter
    if (self.isDestroyed == false) and (self.hitboxes[name] ~= nil) then
      cols, colNumber = self.hitboxes[name]:checkCollision(filter)
      local type = self.hitboxes[name].type

      for i, col in ipairs(cols) do
        self:hitboxResponse(name, type, col)
        if (self.type == "player") and (col.other.owner ~= nil) and (col.other.owner.onPlayerCollision ~= nil) then
            col.other.owner:onPlayerCollision(self, type, col)
        end
      end
    end

    return cols, colNumber
end

-- Debug

function Physics:_drawHitboxes()
    self.mainHitbox:draw()
    for name, value in pairs(self.hitboxes) do
        value:draw()
    end
end

return Physics