-- Hitbox.lua :: a basic 3D hitbox object. It's used by the actors to check
-- collisions and to handle different type of responses.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Hitbox = Object:extend()

local Vector3D = require "framework.libs.brinevector3D"
local physicsUtils = require "framework.scenes.world.actors.physics.utils"

-- INIT FUNCTIONS
-- Initialise the actor and its base functions

function Hitbox:new(owner, type, data, scale, isSolid)
  self.owner    = owner
  self.world    = owner.world

  self.type     = type
  self:setFromData(data, scale)
  self.position = self:getPosition()
  self.isSolid  = isSolid

  self.isMainHitBox = false

  self:register()
end

function Hitbox:advertiseAsMainHitbox()
  self.isMainHitBox = true
end

function Hitbox:modify(position, dimensions)
    self.relativePosition = Vector3D(position.x or 0, position.y or 0, position.z or 0)
    self.dimensions = {w = dimensions.w, h = dimensions.h, d = dimensions.d or 1}
    self.position = self:getPosition()
end

function Hitbox:setFromData(data, scale)
  assert(data.position ~= nil, "data must have a position")
  assert(data.position.x ~= nil, "data must have a x position")
  assert(data.position.y ~= nil, "data must have a y position")
  assert(data.dimensions ~= nil, "data must have a dimension")
  assert(data.dimensions.w ~= nil, "data must have a w dimension")
  assert(data.dimensions.h ~= nil, "data must have a h dimension")

  self.relativePosition = Vector3D(data.position.x, data.position.y, data.position.z or 0)
  self.dimensions = {w = data.dimensions.w, h = data.dimensions.h, d = data.dimensions.d or 1}

  self:applyScale(scale)
end

function Hitbox:applyScale(scale)
  local sx = scale.x or 1
  local sy = scale.y or 1

  if (sx < 0) then
    self.relativePosition.x = self.owner.dimensions.w - self.relativePosition.x - self.dimensions.w
  end

  if (sy < 0) then
    self.relativePosition.y = self.owner.dimensions.h - self.relativePosition.y - self.dimensions.h
  end
end

function Hitbox:register()
  self.world:registerBody(self)
end

function Hitbox:destroy()
  self.world:removeBody(self)
end

-- COORDINATE FUNCTIONS
-- Handle Hitbox position

function Hitbox:updatePosition()
  self.position = self:getPosition()
  self.world:updateBody(self)
  return self.position
end

function Hitbox:getPosition()
    return self.relativePosition + self.owner.position
end

function Hitbox:getOwnerPosition()
    return self.owner.position
end

function Hitbox:getNewOwnerPosition(position)
    return position - self.relativePosition
end

function Hitbox:getCenter()
    return self:getPosition() + ( physicsUtils.dimensionsToVector(self.dimensions) / 2 )
end

-- COLLISION FUNCTIONS
-- Handle Hitbox position

function Hitbox:checkCollision(filter)
  local _, cols, colNumber = self:checkCollisionAtPoint(self.owner.position, filter)
  return cols, colNumber
end

function Hitbox:checkCollisionAtPoint(destination, filter)
  self:updatePosition()

  local tryPosition = self.relativePosition + destination
  local pos, cols, colNumber = self.world:checkCollisionAtPoint(self, tryPosition, filter)
  local new = self:getNewOwnerPosition(pos)

  return new, cols, colNumber
end

-- DEBUG FUNCTIONS
-- Draw debug

function Hitbox:draw()
  local position = self:getPosition()
  local dimensions = self.dimensions
  local r, g, b = love.graphics.getColor()
  love.graphics.setColor(0, 0, 0, 0.5)
  love.graphics.rectangle("fill", math.floor(position.x), math.floor(position.y), dimensions.w, dimensions.h)
  love.graphics.setColor(0, 0, 0, 1)
  love.graphics.rectangle("line", math.floor(position.x), math.floor(position.y), dimensions.w, dimensions.h)
  love.graphics.setColor(r, g, b)
end

return Hitbox
