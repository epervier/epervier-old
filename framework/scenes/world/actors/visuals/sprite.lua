local Sprite = Object:extend()
local Vector3D = require "framework.libs.brinevector3D"

Sprite.is2D = true

-- SPRITES FUNCTIONS
-- Handle the sprite of the actor

function Sprite:new(owner, name, isClone, origin, withCallbacks, getHitboxes)
  self.owner = owner
  self.animators = self.owner.world.animators
  self.name    = name or nil
  self.origin = Vector3D(origin.x or 0, origin.y or 0, 0)
  self.scale = {x = 1, y = 1}
  self.exist   = (name ~= nil)
  self.getHitboxes = getHitboxes
  if (isClone) then
    self:clone()
  end
  if (withCallbacks) then
    self:activateCallbacks()
  end
end

function Sprite:clone()
  if self.name ~= nil then
    self.spriteClone = self.animators:clone(self.name)
  end
end

function Sprite:activateCallbacks()
  if (self.spriteClone ~= nil) then
    self.spriteClone:setCallbackTarget(self.owner)
  end
end

function Sprite:isCloned()
  return (self.spriteClone ~= nil)
end

function Sprite:getAnimator()
  if (self:isCloned()) then
    return self.spriteClone
  else
    return self.animators:get(self.name)
  end
end

function Sprite:changeAnimation(animation, restart)
  if (animation ~= self:getAnimator():getCurrentAnimation()) then
    if (self.getHitboxes) then
      self.owner:purgeHitbox()
    end
  end
  self:getAnimator():changeAnimation(animation, restart)
end

function Sprite:setCustomSpeed(customSpeed)
  self:getAnimator():setCustomSpeed(customSpeed)
end

function Sprite:update(dt)
  if (self.spriteClone ~= nil) then
    self.spriteClone:update(dt)
    self:sendHitboxes()
  end
end

function Sprite:sendHitboxes()
  if (self.getHitboxes) then
    local areas = self:getAnimator():getAreas()
    if (areas ~= nil) then
      self.owner:setHitboxes(areas)
    end
  end
end

function Sprite:setScalling(scale)
    self.scale = {x = scale.x or self.scale.x, y = scale.y or self.scale.y}
end

function Sprite:getCurrentAnimation()
  return self:getAnimator():getCurrentAnimation()
end

function Sprite:getScalling()
  return {x = self.scale.x, y = self.scale.y}
end

function Sprite:getFrame()
  return self:getAnimator():getAbsoluteFrame()
end

function Sprite:getRelativeFrame()
  return self:getAnimator().frame
end

function Sprite:getAnimationDuration()
  return self:getAnimator():getAnimationDuration()
end

function Sprite:draw(position)
    if (self.name == nil) then
        return
    end

    -- TODO: use vector
    local x, y = position.x + self.origin.x, position.y + self.origin.y
    local sx, sy = self.scale.x, self.scale.y

    if (self.spriteClone ~= nil) then
      self.spriteClone:draw(math.floor(x), math.floor(y), 0, sx, sy)
    else
      self.animators:draw(self.name, math.floor(x), math.floor(y), 0, sx, sy)
    end
end

return Sprite