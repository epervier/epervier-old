-- box3D :: drawable box with shadow handling for fake3D actors

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Box3D = Object:extend()

Box3D.is2D = false

-- INITIALISATIONS FUNCTIONS

function Box3D:new(dimensions, texture)
    self.dimensions = { w = dimensions.w, h = dimensions.h, d = dimensions.d or 1 }

    self:_initTexturesAssets(texture)
    self:_regenerate()
end

function Box3D:_initTexturesAssets(texture)
    if (texture == nil) then
        self.color = {r = 1, g = 1, b = 1}
        self.haveLine = true
        return
    end
    if (type(texture) == "string") then
        self.texture = {top = texture, bottom = texture}
        self.topTexture, self.bottomTexture = texture, texture
        self.haveLine = false
    elseif (type(texture) == "table" and type(texture.top) == "string") then
        self.texture = {top = texture.top, bottom = texture.bottom or texture.top}
        self.haveLine = (texture.haveLine == true)
    else
        self.color = {r = texture.r or 1, g = texture.g or 1, b = texture.b or 1}
        self.haveLine = (texture.haveLine ~= false)
    end
end

function Box3D:modify(dimensions)
    self.dimensions = { w = dimensions.w, h = dimensions.h, d = dimensions.d or 1 }

    self:_regenerate()
end

function Box3D:_regenerate()
    local canvas = love.graphics.newCanvas(self.dimensions.w, self.dimensions.h + self.dimensions.d)
    love.graphics.setCanvas(canvas)
    utils.graphics.resetColor()

    local r, g, b = love.graphics.getColor()
    self:drawTextureContent()
    love.graphics.setColor(r, g, b, 1)

    love.graphics.setCanvas()
    local imagedata = canvas:newImageData()
    self.texture = love.graphics.newImage(imagedata)
    imagedata:release()
    canvas:release()
end

function Box3D:drawTextureContent()
    self:drawTopTexture()
    self:drawBottomTexture()
end

function Box3D:drawTopTexture()
    if (self.color ~= nil) then
        love.graphics.setColor(self.color.r, self.color.g, self.color.b, 1)
        love.graphics.rectangle("fill", 0, 0, self.dimensions.w, self.dimensions.h)
    else
        local asset = assets.textures:get(self.texture.top)
        local w, h = asset:getDimensions()
        local sx = self.dimensions.w / w
        local sy = self.dimensions.h / h
        love.graphics.draw(asset, 0, 0, 0, sx, sy)
    end
end

function Box3D:drawBottomTexture()
    if (self.color ~= nil) then
        love.graphics.setColor(self.color.r * 0.9, self.color.g * 0.9, self.color.b * 0.9, 1)
        love.graphics.rectangle("fill", 0, self.dimensions.h, self.dimensions.w, self.dimensions.d)
    else
        local asset = assets.textures:get(self.texture.bottom)
        local w, h = asset:getDimensions()
        local sx = self.dimensions.w / w
        local sy = self.dimensions.d / h
        love.graphics.draw(asset, 0, self.dimensions.h, 0, sx, sy)
    end
end

function Box3D:draw(position)
    local x, y = position.x, (position.y - position.z) - (self.dimensions.d)
    love.graphics.setColor(0, 0, 0, 1)
    if (self.haveLine) then
        love.graphics.rectangle("line", x, y, self.dimensions.w, self.dimensions.d + self.dimensions.h)
    end
    utils.graphics.resetColor()
    love.graphics.draw(self.texture, x, y)
end

return Box3D
