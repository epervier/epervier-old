local Rectangle = require "framework.scenes.world.actors.visuals.rectangle"
local Sprite = require "framework.scenes.world.actors.visuals.sprite"
local Box3D = require "framework.scenes.world.actors.visuals.boxes"
local Visual = Object:extend()

local Vector3D = require "framework.libs.brinevector3D"
local physicsUtils = require "framework.scenes.world.actors.physics.utils"

function Visual:_initVisual(def)
    self.visualMode = def.mode or ""
    if (self.visualMode == "box") then
        self:_initBox(def.color or {r = 0, g = 0, b = 0})
    elseif (self.visualMode == "sprite") then
        self:_initSprite(def.assetName, def.clone == true, def.origin or {}, def.withCallbacks == true, def.getHitboxes == true)
    elseif (self.visualMode == "box3D") then
        self:_initBox3D(self.dimensions, def.texture or def.color)
    end

    if (self.visual ~= nil) then
        self.world:registerShape(self)
    end
end

function Visual:_removeVisual()
    if (self.visual ~= nil) then
        self.visual = nil
        self.world:removeShape(self)
    end
end

function Visual:_updateVisual(dt)
    if (self.visualMode == "sprite") then
        self.visual:update(dt)
    end
end

function Visual:_getShapeData()
    local position = self.position:clone()
    local dimensions = {
        w = self.dimensions.w,
        h = self.dimensions.h,
        d = self.dimensions.d
    }

    if (self.world.type == "3D") then
        position.y = (position.y - position.z) - self.dimensions.d
        dimensions.h = self.dimensions.h + self.dimensions.d
        if (self.zGround ~= nil) then
            dimensions.h = dimensions.h + (position.z - self.zGround)
        end
    end
    return position, dimensions
end

function Visual:_getViewCenter(mode)
    local position = self.position + (physicsUtils.dimensionsToVector(self.dimensions) / 2)
    if (self.world.type == "3D") then
        position.y = position.y - (self.dimensions.d / 2)
    end

    return position
end

function Visual:_drawVisual()
    local position, dimensions = self.position:clone(), self.dimensions
    if (self.world.type == "3D" and self.visual.is2D) then
        position, dimensions = self:_getShapeData()
        position.y = position.y + self.dimensions.h * (3 / 4)
    end
    self.visual:draw(position, dimensions);
end

-- RECT HANDLING
-- Handle the rect mode

function Visual:_initBox(color)
    self.visual = Rectangle(color)
end

-- BOX3D HANDLING
-- Handle the 3D mode

function Visual:_initBox3D(dimension, texture)
    self.visual = Box3D(dimension, texture)
end

-- SPRITE HANDLING
-- Handle the sprite mode for visual

function Visual:_initSprite(name, isClone, origin, withCallbacks, getHitboxes)
    self.visual = Sprite(self, name, isClone, origin, withCallbacks, getHitboxes)
end

-- TILE HANDLING
-- Handle the tile mode for visual

-- TEXTURE HANDLING
-- Handle the texture mode for visual

-- BOX HANDLING
-- TODO


return Visual