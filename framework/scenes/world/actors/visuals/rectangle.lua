local Rectangle = Object:extend()

Rectangle.is2D = true

function Rectangle:new(color)
    self.color = {r = color.r or 0, g = color.g or 0, b = color.b or 0}
end

function Rectangle:update(dt)

end

function Rectangle:draw(position, dimensions)
    local r, g, b = love.graphics.getColor()
    love.graphics.setColor(self.color.r, self.color.g, self.color.b, 0.5)
    love.graphics.rectangle("fill", math.floor(position.x), math.floor(position.y), dimensions.w, dimensions.h)
    love.graphics.setColor(self.color.r, self.color.g, self.color.b, 1)
    love.graphics.rectangle("line", math.floor(position.x), math.floor(position.y), dimensions.w, dimensions.h)
    love.graphics.setColor(r, g, b)
end

return Rectangle