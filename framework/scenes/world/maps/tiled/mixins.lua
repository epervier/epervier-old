local TiledMixins = Object:extend()

-- ACTOR ADDING FUNCTIONS
-- Add actor related to the map

function TiledMixins:batchActor(objectlayer, object, layerx, layery)
    local name = objectlayer.name
    local gwidth = object.properties.gwidth or self.wrapper.sti.tilewidth
    local gheight = object.properties.gheight or self.wrapper.sti.tileheight
    local x = object.x + layerx
    local y = object.y + layery
    local z = object.properties.z or 0
    local w = object.width
    local h = object.height

    local cellHor = math.ceil(w / gwidth)
    local cellVert = math.ceil(h / gheight)

    for i = 1, cellHor do
        for j = 1, cellVert do
            self.world:newActor(name, x + (i - 1) * gwidth, y + (j - 1) * gheight, z, object.properties)
        end
    end
end

function TiledMixins:newActor(objectlayer, object, layerx, layery)
    local z = object.properties.z or 0
    local adaptPosition = object.properties.adaptPosition or false

    local x = object.x + layerx
    local y = object.y + layery
    if (adaptPosition) then
        y = y + z
    end

    self.world:newActor(objectlayer.name, x, y, z, object.properties, self.mapname)
end

function TiledMixins:newCollision(objectlayer, object, layerx, layery)
    local z = object.properties.z or 0
    local d = object.properties.d or 16
    local fromTop = object.properties.fromTop or false
    local x = object.x + layerx
    local y = object.y + layery
    if (fromTop) then
        local poschange = z .. ";" .. y .. " => " .. z - d .. ";" .. y + z
        core.debug:print("map/sti", "create from top, set z and y: " .. poschange)
        y = y + z
        z = z - d
    end

    self.world:newCollision(objectlayer.name, x, y, z, object.width, object.height, d)
end

function TiledMixins:newPlayer(object, i, layerx, layery)
    local z = object.properties.z or 0
    local adaptPosition = object.properties.adaptPosition or false

    local x = object.x + layerx

    local y = object.y + layery
    if (adaptPosition) then
        core.debug:print("map/sti", "adapting position, set y: " .. y .. " => ", y + z)
        y = y + z
    end

    self.world:addPlayer(x, y, z, i)
end

-- INDEXATION FUNCTIONS
-- Verify if something is indexed

function TiledMixins:isCollisionIndexed(name)
    return self.world:isCollisionIndexed(name)
end

function TiledMixins:isActorIndexed(name)
    return self.world:isActorIndexed(name)
end

return TiledMixins
