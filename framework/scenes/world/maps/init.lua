local cwd = (...):gsub('%.init$', '') .. "."

local mapObjects = {}
mapObjects.Sti    = require(cwd .. "tiled")
mapObjects.Base   = require(cwd .. "parent")

return mapObjects
