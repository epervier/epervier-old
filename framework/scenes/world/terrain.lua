-- mapped.lua :: a sti-mapped box

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local MappedBox = extend "framework.scenes.world.actors.visuals.boxes"
local Vector3D = require "framework.libs.brinevector3D"

function MappedBox:new(world, position, dimensions)
  self.world = world
  self.position = Vector3D(position.x, position.y, position.z or 0)
  self.depth = -1

  MappedBox.super.new(self, dimensions)
  self.haveLine = false
end

function MappedBox:drawTextureContent()
  local tx, ty = self.position.x, self.position.y - (self.position.z + self.dimensions.d)
  core.debug:print("mappedbox", "getting map layers at position " .. tx .. ";" .. ty)
  love.graphics.push()
  love.graphics.origin()
  love.graphics.translate(math.floor(-tx), math.floor(-ty))

  self.world:drawMap()

  love.graphics.pop()
end

function MappedBox:getShape()
  local position = self.position:clone()
  local dimensions = {
      w = self.dimensions.w,
      h = self.dimensions.h,
      d = self.dimensions.d
  }

  position.y = (position.y - position.z) - self.dimensions.d
  dimensions.h = self.dimensions.h + self.dimensions.d
  return position, dimensions
end

function MappedBox:draw()
  MappedBox.super.draw(self, self.position)
end


return MappedBox
