local Sorter = Object:extend()

local function _depthSort(itemA, itemB)
    local aDepth, aID = itemA.depth, itemA.creationID
    local bDepth, bID = itemB.depth, itemB.creationID
    local comparison = 0
    if aDepth > bDepth then
        comparison = 1
    elseif aDepth < bDepth then
        comparison = -1
    else
        if aID > bID then
            comparison = 1
        elseif aID < bID then
            comparison = -1
        end
    end
    return comparison == -1
end

local function _zSort(itemA, itemB)
    local positionA, positionB = utils.vector.floor(itemA.position), utils.vector.floor(itemB.position)

    local aY, aZ, aH, aD = positionA.y, itemA.zGround or positionA.z, itemA.dimensions.h, itemA.dimensions.d
    local bY, bZ, bH, bD = positionB.y, itemB.zGround or positionB.z, itemB.dimensions.h, itemB.dimensions.d

    local comparison = 0
    local compareString = ""

    if aZ >= bZ + bD then
        -- item A is completely above item B
        compareString = "est complètement au dessus"
        comparison = 1
    elseif bZ >= aZ + aD then
        -- item B is completely above item A
        compareString = "est complètement en dessous"
        comparison = -1
    elseif aY + aH <= bY then
        -- item A is completely behind item B
        compareString = "est complètement derrière"
        comparison = -1
    elseif bY + bH <= aY then
        -- item B is completely behind item A
        compareString = "est complètement devant"
        comparison = 1
    elseif aY + aZ + aH + aD >= bY + bZ + bH + bD then --(aY - aZ) + aH > (bY - bZ) + bH then
        -- item A's forward-most point is in front of item B's forward-most point
        compareString = "à son point le plus en avant (y+h) devant celui"
        comparison = 1
    elseif aY + aZ + aH + aD <= bY + bZ + bH + bD then --aY < (bY - bZ) + bH then
        -- item B's forward-most point is in front of item A's forward-most point
        compareString = "à son point le plus en avant (y+h) derrière celui"
        comparison = -1
    else
        -- item A's forward-most point is the same than item B's forward-most point
        compareString = "est au même niveau global"
        return _depthSort(itemA, itemB)
    end

    if (love.keyboard.isDown("d")) then
        print((itemA.type or "nil") ..
        itemA.creationID .. " " .. (compareString .. (" de " .. (itemB.type or "nil"))) .. itemB.creationID)
    end

    return comparison == -1
end

function Sorter:new(world)
    self.world = world
    self.mode = world.type or "2D"
end

function Sorter:sort(items)
    if (self.mode == "2D") then
        self:sort2D(items)
    else
        self:sort3D(items)
    end
end

function Sorter:sort2D(items)
    table.sort(items, _depthSort)
end

function Sorter:sort3D(items)
    table.sort(items, _zSort)
end

return Sorter
