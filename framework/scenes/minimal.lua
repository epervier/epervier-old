-- scenes.lua :: the scene object, that aim to give a better control to the engine
-- to the different scene, without having to call too much boilerplate
-- Minimal version of the scene system, that will be used for the future version
-- of scenes

--[[
  Copyright © 2024 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Scene  = Object:extend()
local AnimatorManager = require "framework.scenes.animators"

function Scene:new()
    self.mouse = {}
    self.mouse.x, self.mouse.y = core.screen:getMousePosition()
  
    self.animators  = AnimatorManager()
  
    self.dialog = nil

    self:register()
end

function Scene:register()
  core.scenemanager:setScene(self)
end

function Scene:updateScene(dt)
    self.animators:update(dt)
    self:update(dt)
end

function Scene:drawScene()
    self:draw()
    core.screen:drawTransition()
    self:drawOverTransition()
end

-- Callbacks

function Scene:init(data)
    -- Empty function, can be replaced by callbacks
end

function Scene:restored(data)
    -- Empty function, can be replaced by callbacks
end

function Scene:update(dt)
    -- Empty function, can be replaced by callbacks
end

function Scene:mousemoved(x, y, dx, dy)
    -- Empty function, can be replaced by callbacks
end

function Scene:mousepressed( x, y, button, istouch )
    -- Empty function, can be replaced by callbacks
end

function Scene:keypressed( key, scancode, isrepeat )
    -- Empty function, can be replaced by callbacks
end

function Scene:keyreleased( key )
    -- Empty function, can be replaced by callbacks
end

function Scene:redraw()
    -- Empty function, can be replaced by callbacks
end

function Scene:draw()
    -- Empty function, can be replaced by callbacks
end

function Scene:drawOverTransition()
    -- Empty function, can be replaced by callbacks
end

function Scene:clear()
    -- TODO: send automatic cleanups to the different elements of the scene
end

return Scene