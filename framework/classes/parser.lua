-- parser.lua :: a data parser, used to parse data from arguments

--[[
  Copyright © 2021 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Parser = Object:extend()

function Parser:new(headings, argumentsList, argumentwrapper, nullableNbr)
    self.headings = headings
    self.argumentsList = argumentsList
    self.argumentwrapper = argumentwrapper or ""
    self.nullableNbr = nullableNbr or 0
end

function Parser:getArguments(name)
    if (self.argumentsList[name] ~= nil) then
        local arguments = {}
        utils.table.mergeList(arguments, self.headings)
        utils.table.mergeList(arguments, self.argumentsList[name])
        return arguments
    else
        error("No arguments data for argumentList " .. name)
    end
end

function Parser:parse(datas)
    local arguments = self:getArguments(datas[1])
    --print(utils.table.toString(arguments))
    local parsedData = utils.table.parse(datas, arguments, self.nullableNbr)
    if (utils.string.isEmpty(self.argumentwrapper)) then
        -- print(utils.table.toString(parsedData))
        return parsedData
    else
        local finalTable = {}
        for i, heading in ipairs(self.headings) do
            finalTable[heading] = parsedData[heading]
            parsedData[heading] = nil
        end
        finalTable[self.argumentwrapper] = parsedData
        -- print(utils.table.toString(finalTable))
        return finalTable
    end
end

return Parser