
-- time.lua :: a timer, tweener and timed switch handler.
-- This class need framework.libs.tween to work

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local TweenManager = Object:extend()

local tween = require "framework.libs.tween"
local Timer = require "framework.classes.time.timer"
local TimedFunction = require "framework.classes.time.func"

function TweenManager:new(subject)
  self.subject  = subject
  self.time    = 0

  self.tweens   = {}
  self.switches = {}

  self.timers   = {}
  self.funcs    = {}
end

function TweenManager:newTween(start, duration, target, easing)
  table.insert(self.tweens, self:createTween(start, duration, target, easing))
end

function TweenManager:setNamedTween(name, start, duration, target, easing)
  self.tweens[name] = nil
  self.tweens[name] = self:createTween(start, duration, target, easing)
end

function TweenManager:createTween(start, duration, target, easing)
  local newTween = {}
  -- we add the data into a tween wrapper
  newTween.tween = tween.new(duration, self.subject, target, easing)
  newTween.start = self.time + start -- /!\ START IS RELATIVE TO CURRENT TIME
  newTween.clear = newTween.start + duration

  return newTween
end

function TweenManager:removeNamedTween(name)
  self.tweens[name] = nil
end

function TweenManager:haveTween()
  return #self.tweens > 0
end

-- TIMER FUNCTIONS
-- Help to get info from timers

function TweenManager:newTimer(start, name)
  self.timers[name] = Timer(self, name, start)
end

function TweenManager:delayTimer(time, name, absolute)
  if (self.timers[name] ~= nil) then
    self.timers[name]:delay(time, absolute)
  end
end

function TweenManager:getTimerInfo(name)
  if (self.timers[name] ~= nil) then
    return self.timers[name]:getInfo()
  end
end

function TweenManager:removeTimer(name)
  self.timers[name] = nil
end

-- FUNCTION FUNCTIONS
-- Help to get functions

function TweenManager:newFunc(start, name, func)
  self.funcs[name] = TimedFunction(self, name, func, start)
end

function TweenManager:delayFunc(time, name, absolute)
  if (self.funcs[name] ~= nil) then
    self.funcs[name]:delay(time, absolute)
  end
end

function TweenManager:getFuncInfo(name)
  if (self.funcs[name] ~= nil) then
    return self.funcs[name]:getInfo()
  end
end

function TweenManager:removeFunc(name)
  self.funcs[name] = nil
end

-- SWITCH FUNCTIONS
-- Help to handle switches

function TweenManager:newSwitch(start, bools)
  local newSwitch = {}
  -- we add the data into a tween wrapper
  newSwitch.bools = bools
  newSwitch.start = self.time + start -- /!\ START IS RELATIVE TO CURRENT TIME
  newSwitch.clear = newSwitch.start + 1

  table.insert(self.switches, newSwitch)
end

function TweenManager:update(dt)
  self.time = self.time + dt

  for key, tweenWrapper in pairs(self.tweens) do
    if (self.time > tweenWrapper.start) then
      tweenWrapper.tween:update(dt)
      if (self.time > tweenWrapper.clear) then
        self.tweens[key] = nil
      end
    end
  end

  for i, switch in pairs(self.switches) do
    if (self.time > switch.start) then
      -- We test each boolean in the switch
      for i, bool in ipairs(switch.bools) do
        -- if it's nil, we set it to true
        if self.subject[bool] == nil then
          self.subject[bool] = true
        else
          -- if it's not nil, we reverse the boolean
          self.subject[bool] = (self.subject[bool] == false)
        end
      end
      table.remove(self.switches, i)
    end
  end

  for k, timer in pairs(self.timers) do
    timer:update(dt)
  end

  for k, func in pairs(self.funcs) do
    func:update(dt)
  end

  self:clearEndedTweens()
end

function TweenManager:timerResponse(timername)
  if self.subject.timerResponse == nil then
    core.debug:warning("tweenmanager", "the subject have no timerResponse function")
    return 0
  end
  self.subject:timerResponse(timername)
end

function TweenManager:clearEndedTweens(dt)
  for i, tweenWrapper in ipairs(self.tweens) do
    if (self.time > tweenWrapper.clear) then
      table.remove(self.tweens, i)
    end
  end
end

return TweenManager
