local Timer = require "framework.classes.time.timer"
local TimedFunction = Timer:extend()

function TimedFunction:new(actor, name, func, t)
    TimedFunction.super.new(self, actor, name, t)
    self.func = func
end

function TimedFunction:finish()
    self.actor.funcs[self.name] = nil
    self.func()
end

return TimedFunction