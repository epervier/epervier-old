-- classes/serializable :: a serializable object, can give its field as data

--[[
  Copyright © 2021 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Serializable = Object:extend()

function Serializable:new(serializeFields, listSerializable)
    self.serializeFields = serializeFields
    self.listSerializable = listSerializable
end

function Serializable:getData()
    local data = {}
    -- We serialize all fields
    if (self.serializeFields ~= nil) then
        for _, key in ipairs(self.serializeFields) do
            data[key] = self:wrapField(key)
        end
    end
    -- We serialize all list of serializable 
    if (self.listSerializable ~= nil) then
        for _, key in ipairs(self.listSerializable) do
            data[key] = self:wrapList(key)
        end
    end
    return data
end

function Serializable:wrapField(key)
    local serializedField = self[key]
    if (serializedField ~= nil) then
        if (type(serializedField) == "table" and serializedField.is ~= nil) then
            if (serializedField:is(Serializable)) then
                serializedField = serializedField:getData()
                serializedField.isSerializable = true
            end
        end
        self:print(key, serializedField, false)
        return serializedField
    end
end

function Serializable:wrapList(key)
    local serializedList = {}
    serializedList.isListSerializable = true
    for listKey, listElement in pairs(self[key]) do
        serializedList[listKey] = listElement:getData()
    end
    self:print(key, serializedList, false)
    return serializedList
end

function Serializable:setData(data)
    for key, value in pairs(data) do
        if (key ~= "serializedField") then
            self:unwrapField(key, value)
        end
    end
    self:finishDeserialization()
end

function Serializable:unwrapField(key, value)
    local serializedField = value
    if (serializedField ~= nil) then
        if (type(serializedField) == "table") then
            if (serializedField.isSerializable == true) then
                self[key]:setData(serializedField)
            elseif (serializedField.isListSerializable) then
                self:unwrapList(key, serializedField)
            else
                self[key] = serializedField or self[key]
            end
        else
            self[key] = serializedField or self[key]
            self:print(key, serializedField, true)
        end
    end
end

function Serializable:unwrapList(key, list)
    if (list == nil) then
        error("List " .. key .. " shouldn't be null in the save")
    end
    for listKey, listElement in pairs(list) do
        if (listKey ~= "isListSerializable") then
            self[key][listKey]:setData(listElement)
        end
    end
    self:print(key, list, true)
end

function Serializable:print(key, value, isLoading)
    local loadString = "Loading "
    if (isLoading == false) then
        loadString = "Saving "
    end
    local valueString = value
    if type(valueString) == "boolean" then
        if valueString then
            valueString = "true"
        else
            valueString = "false"
        end
    end
    if (type(value) == "table") then
        valueString = utils.table.toString(value)
    end
    loadString = loadString .. key .. " " .. ": " .. valueString
    if (core ~= nil) then
        core.debug:debug("serializable", loadString)
    else
        --print("serializable", loadString)
    end
end

function Serializable:finishDeserialization()
    -- Empty function callback
end

return Serializable