-- classes/serializable/serializer :: a serializer, able to put stuff into a bin file

--[[
  Copyright © 2021 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]
local Serializable = require "framework.classes.serializable"
local Serializer = Serializable:extend()
local binser = require("framework.libs.binser")

function Serializer:new(serializeFields, listSerializable)
    Serializer.super.new(self, serializeFields, listSerializable)
end

function Serializer:reset()

end

function Serializer:delete(filename)
    local filepath = self:getFile(true, filename)
    if utils.filesystem.exists(filename) then
        love.filesystem.remove(filepath)
    end
end

function Serializer:deserialize(filename)
    local filepath = self:getFile(true, filename)
    if utils.filesystem.exists(filename) then
        local loadedDatas = binser.readFile(filepath)
        self:setData(loadedDatas[1])
    else
        self:reset()
    end
end

function Serializer:serialize(filename)
    local data = self:getData()

    local filepath = self:getFile(true, filename)
    binser.writeFile(filepath, data)
end

function Serializer:getFile(absolute, filename)
    local dir = ""
    if absolute then
        dir = love.filesystem.getSaveDirectory() .. "/"
        if (not utils.filesystem.exists(dir)) then
            love.filesystem.createDirectory("")
        end
    end

    local filepath = dir .. filename

    return filepath
end

return Serializer
