-- classes/simple :: the actual solver of the predicate system. It parse a condition
-- system, and pass the data to a solver function. If the last argument of a condition
-- is "not", it'll negate the whole condition.

-- Each solver function will get as argument the list of conditions, the predicate and the
-- asker. The predicate contain an utility subclass with some function to handle easily 
-- some stuff like converting truth tags or comparing numbers
-- An empty string will return true, a non-existing solver question an error, except if the solver have a
-- default function.

--[[
  Copyright © 2021 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local SimplePredicate = Object:extend()
local SEPARATOR = ":"
local NOT = "not"
local DEFAULT = "default"

SimplePredicate.utils = require "framework.classes.predicate.utils"

function SimplePredicate:new(data, solver, asker)
    self.solver = solver
    self.asker = asker
    self.data = data
end

function SimplePredicate:solve()
    if (utils.string.isEmpty(self.data)) then
      return true
    end
    local conditionArgs = utils.string.split(self.data, SEPARATOR)
    local solverFunction = self:getFunction(conditionArgs[1])
    local conditionFulfilled = solverFunction(conditionArgs, self, self.asker)
    if (conditionArgs[#conditionArgs] == NOT) then
        conditionFulfilled = (not conditionFulfilled)
    end
    return conditionFulfilled
end

function SimplePredicate:getFunction(functionName)
    if (self.solver[functionName] == nil) then
        if (self.solver[DEFAULT] == nil) then
            error("Function " .. functionName .. " doesn't exist in solver, and it doesn't have a 'default' function.")
        end
        return self.solver[DEFAULT]
    end
    return self.solver[functionName]
end

return SimplePredicate