-- rect.lua :: a 2D rectangle.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Point = require "framework.classes.2D.point"
local Rect = Point:extend()

function Rect:new(x, y, w, h)
    Rect.super.new(self, x, y)
    self:setSize(w, h)
end

function Rect:set(x, y, w, h)
    self:setPosition(x, y)
    self:setSize(w, h)
end

function Rect:setSize(w, h)
    self.w = w or self.w
    self.h = h or self.h
end

function Rect:getArea()
    local x, y = self:getPosition()
    return x, y, self.w, self.h
end

function Rect:getShape()
  return self:getArea()
end

function Rect:getCorners()
    local x, y, w, h = self:getArea()
    return x, y, x + w, y + h
end

function Rect:getCenter()
    local x, y, w, h = self:getArea()
    return math.floor(x + (w/2)), math.floor(y + (h/2))
end

function Rect:areCoordInside(dx, dy)
    local x, y, w, h = self:getArea()
    return ((dx > x) and (dx < x + w) and (dy > y) and (dy < y + h))
end

function Rect:getRelativeCoordinate(dx, dy)
    return dx - self.x, dy - self.y
end

function Rect:getDimensions()
    return self.w, self.h
end

function Rect:drawBox()
    utils.graphics.box(self.x, self.y, self.w, self.h)
end

return Rect