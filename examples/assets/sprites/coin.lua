return {
  metadata = {
    height      = 16,
    width       = 16,
    defaultAnim = "anim"
  },
  animations = {
    ["anim"] = {
      startAt     = 1,
      endAt       = 4,
      loop        = 1,
      speed       = 6,
      pauseAtEnd  = false,
    }
  }
}
