return {
  metadata = {
    height      = 16,
    width       = 16,
    defaultAnim = "anim"
  },
  animations = {
    ["anim"] = {
      startAt     = 1,
      endAt       = 5,
      loop        = 1,
      speed       = 10,
      pauseAtEnd  = false,
    }
  }
}
