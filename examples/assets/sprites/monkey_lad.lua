return {
  metadata = {
    height      = 24,
    width       = 32,
    ox          = 16,
    oy          = 12,
    defaultAnim = "idle"
  },
  animations = {
    ["idle"] = {
      startAt     = 1,
      endAt       = 1,
      loop        = 1,
      speed       = 0,
      pauseAtEnd  = false,
    },
    ["walk"] = {
      startAt     = 3,
      endAt       = 6,
      loop        = 3,
      speed       = -1,
      pauseAtEnd  = false,
    },
    ["jump"] = {
      startAt     = 7,
      endAt       = 7,
      loop        = 1,
      speed       = 0,
      pauseAtEnd  = false,
    },
    ["brake"] = {
      startAt     = 7,
      endAt       = 7,
      loop        = 1,
      speed       = 0,
      pauseAtEnd  = false,
    },
    ["climb"] = {
      startAt     = 8,
      endAt       = 9,
      loop        = 1,
      speed       = -1,
      pauseAtEnd  = false,
    },
    ["punch"] = {
      startAt     = 11,
      endAt       = 11,
      loop        = 1,
      speed       = 0,
      pauseAtEnd  = false,
      areas = {
        ["frame1"] = {{type = "punch", box = {position = {x = 16, y = 6}, dimensions = {w = 12, h = 12}}, isSolid = false}}
      }
    },
  }
}
