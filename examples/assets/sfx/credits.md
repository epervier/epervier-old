# Credits

## Mateus Reis

- break.wav (CC-BY-SA 3.0)

- checkpoint.wav (CC-BY-SA 3.0)

- jump.wav (CC-BY-SA 3.0)

- spring.wav (CC-BY-SA 3.0)

## SilverstepP

- shield.wav (GPL)

- acidshield.wav (GPL)

- fireshield.wav (GPL)

- thundershield.wav (GPL)

- watershield.wav (GPL)

- windshield.wav (GPL)

## Alexandre

- spikes_appearing.wav (CC-BY-SA 3.0)

- spikes_disappearing.wav (CC-BY-SA 3.0)

## Supertux

- collectcoin.wav (GPL v2)
