## Assets credits

- SFX by [Joth](http://opengameart.org/users/joth)

- Option Music from [OpenSonic](http://opensnc.sourceforge.net/home/index.php) by Di Rodrigues

- Fonts from [SuperPowers](https://opengameart.org/content/superpowers-assets-bitmap-fonts)

### Sprites

- [Monkey Lad in Magical Planet](https://opengameart.org/content/monkey-lad-in-magical-planet) by [surt](https://opengameart.org/users/surt)

- [Tux Bros](https://opengameart.org/content/tux-bros) by [surt](https://opengameart.org/users/surt)

- [16x16 Fantasy Tileset](https://opengameart.org/content/16x16-fantasy-tileset) by [Jerom](https://opengameart.org/users/jerom)

- [Arcade Plateformer Assets](https://opengameart.org/content/arcade-platformer-assets) by [GrafxKid](https://opengameart.org/users/grafxkid)
