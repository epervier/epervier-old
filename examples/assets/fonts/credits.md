# Imperium Porcorum

## Fonts credits

- **Good Neighbors** by Clint Bellanger (Licence CC0), found on [OpenGameArt](http://opengameart.org/content/good-neighbors-pixel-font)

- **Boxy Bold Font+** by usr_share and Clint Bellanger (Licence CC0), found on [OpenGameArt](http://opengameart.org/content/boxy-bold-font-0)

- **Pixel Operator** by HervettFox (Licence CC0), found on [NotABug](https://notabug.org/HarvettFox96/ttf-pixeloperator)
