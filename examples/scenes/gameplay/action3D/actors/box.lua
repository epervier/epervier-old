return actor {
  type = "box",
  dimensions = {w = 16, h = 16, d = 16},
  visuals = {
    mode = "box3D",
    texture = {top = "boxtop", bottom = "box"}
  },
  isSolid = true
}