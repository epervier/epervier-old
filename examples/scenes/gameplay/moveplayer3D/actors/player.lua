local Player = actor {
  type = "player",
  dimensions = { w = 16, h = 16, d = 24 },
  friction = { x = 480 * 3, y = 480 * 3 },
  gravity = { axis = "z", value = -480 },
  isSolid = true,
  visuals = {
    mode = "sprite",
    assetName = "monkey_lad",
    clone = true,
    origin = {
      x = 8,
      y = 12
    },
    getHitboxes = true
  },
}

function Player:onInit()
  self.coin = 0
end

function Player:update(dt)
  if love.keyboard.isDown("up") and (not self.isPunching) then
    self.speed.y = -120
  end
  if love.keyboard.isDown("down") and (not self.isPunching) then
    self.speed.y = 120
  end
  if love.keyboard.isDown("left") and (not self.isPunching) then
    self.speed.x = -120
  end
  if love.keyboard.isDown("right") and (not self.isPunching) then
    self.speed.x = 120
  end

  if love.keyboard.isDown("a") and (self.onGround) then
    self.speed.z = 280
    assets:playSFX("gameplay.jump")
  end

  if love.keyboard.isDown("z") then
    self.isPunching = true
  else
    self.isPunching = false
  end

  self:setAnimation()
end

function Player:setAnimation()
  local gsp = utils.math.pointDistance(0, 0, self.speed.x, self.speed.y)
  self.visual:setCustomSpeed(math.abs(gsp) / 12)
  self:setDirection(self.speed.x)
  if (self.isPunching) then
    self.visual:changeAnimation("punch", false)
  else
    if (self.onGround) then
      if (math.abs(self.speed.x) > 0) or (math.abs(self.speed.y) > 0) then
        self.visual:changeAnimation("walk", false)
      else
        self.visual:changeAnimation("idle", true)
      end
    else
      self.visual:changeAnimation("jump", true)
    end
  end
end

function Player:setDirection(direction)
  direction = direction or 0
  if direction ~= 0 then
    direction = utils.math.sign(direction)
    self.direction = direction
    self.visual:setScalling({ x = direction, y = nil })
  end
end

function Player:drawHUD()
  assets:print("medium", "Coins : " .. self.coin, 8, 8)
end

return Player
