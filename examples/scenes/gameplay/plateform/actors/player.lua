local Player = actor {
  type = "player",
  dimensions = { w = 16, h = 24 },
  friction = { x = 480 * 3 },
  isSolid = true,
  visuals = {
    mode = "sprite",
    assetName = "monkey_lad",
    clone = true,
    origin = {
      x = 8,
      y = 12
    },
    getHitboxes = true
  },
  gravity = { axis = "y", value = 480 },
  drawHitboxes = true
}

function Player:onInit()
  self.isDead = false
  self.start = self.position:clone()
  self.isPunching = false
  self.direction = 1
  self.coin = 0
  self.punchName = ""
end

function Player:update(dt)
  if love.keyboard.isDown("down") then
    self.mainHitbox:modify({x = 0, y = 8}, {w = 16, h = 16})
  else
    self.mainHitbox:modify({x = 0, y = 0}, {w = 16, h = 24})
  end
  if love.keyboard.isDown("left") and (not self.isPunching) then
    self.speed.x = -120
  end
  if love.keyboard.isDown("right") and (not self.isPunching) then
    self.speed.x = 120
  end

  if love.keyboard.isDown("a") and (self.onGround) then
    self.speed.y = -280
    assets:playSFX("gameplay.jump")
  end

  if love.keyboard.isDown("z") then
    self.isPunching = true
  else
    self.isPunching = false
  end

  self:setDirection(self.speed.x)
  self:setAnimation()

  local _, dimensions = self.world:getBox()

  if (self.position.y > (dimensions.h + self.dimensions.h)) and (self.isDead == false) then
    self.timers:newTimer(1, "respawn")
    self.isDead = true
  end
end

function Player:setAnimation()
  self.visual:setCustomSpeed(math.abs(self.speed.x) / 12)
  if (self.isPunching) then
    self.visual:changeAnimation("punch", false)
  else
    if (self.onGround) then
      if math.abs(self.speed.x) > 0 then
        self.visual:changeAnimation("walk", false)
      else
        self.visual:changeAnimation("idle", true)
      end
    else
      self.visual:changeAnimation("jump", true)
    end
  end
end

function Player:setDirection(direction)
  direction = direction or 0
  if direction ~= 0 then
    direction = utils.math.sign(direction)
    self.direction = direction
    self.visual:setScalling({x = direction})
  end
end

function Player:timerResponse(timer)
  if timer == "respawn" then
    self.position.x, self.position.y = self.start.x, self.start.y
    self.speed.x, self.speed.y = 0, 0
    self.isDead = false
  end
end

function Player:drawHUD()
  assets:print("medium", "Coins : " .. self.coin, 8, 8)
end

return Player
