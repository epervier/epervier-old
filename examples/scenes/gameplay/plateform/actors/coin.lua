return actor {
  type = "coin",
  dimensions = { w = 16, h = 16 },
  isSolid = false,
  visuals = {
    mode = "sprite",
    assetName = "coin"
  },
  onPlayerCollision = function (self, player)
      self:destroy()
      assets:playSFX("gameplay.collectcoin")
      self.world:showGFX("gfx.sparkle", self.position)
      player.coin = player.coin + 1
  end
}
