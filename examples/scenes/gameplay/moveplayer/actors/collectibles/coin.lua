return actor {
    type = "coin",
    dimensions = {w = 16, h = 16},
    isSolid = false,
    visuals = {
      mode = "box",
      color = {r = 1, g = 1, b = 0}
    },
    onPlayerCollision = function (self, player)
        self:destroy()
        assets:playSFX("gameplay.collectcoin")
        player.coin = player.coin + 1
    end
}