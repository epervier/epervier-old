
local Player = actor {
  type = "player",
  dimensions = {w = 16, h = 16},
  friction = {x = 480*3, y = 480*3},
  isSolid = true,
  visuals = {
    mode = "box",
    color = {r = .5, g = 0, b = 0}
  }
}

function Player:onInit()
  self.coin = 0
end

function Player:update(dt)
  if love.keyboard.isDown("up") then
    self.speed.y = -120
  end
  if love.keyboard.isDown("down") then
    self.speed.y =  120
  end
  if love.keyboard.isDown("left") then
    self.speed.x = -120
  end
  if love.keyboard.isDown("right") then
    self.speed.x =  120
  end
end

function Player:drawHUD()
  assets:print("medium", "Coins : " .. self.coin, 8, 8)
end

return Player
