local Parent = require "framework.scenes.gui.textmenu"
local MainMenu = Parent:extend()

local defTransitions = require "framework.scenes.transitions"

local MENU_X, MENU_Y = 24, 48
local MENU_W = 424/3
local MENU_ITEM_NUMBER = 8

function MainMenu:new()
    MainMenu.super.new(self, "mainmenu", "medium", MENU_X, MENU_Y, MENU_W, MENU_ITEM_NUMBER, false)
    self:addItem("Plateformer", "left", function() self:launchScene(scenes.Plateformer, 1) end)
    self:addItem("Overworld", "left", function() self:launchScene(scenes.MovePlayer, 1) end)
    self:addItem("Basic 3D", "left", function() self:launchScene(scenes.MovePlayer3D, 1) end)
    self:addItem("Complexe 3D", "left", function() self:launchScene(scenes.Action3D, 1) end)

    self:switch("main")
    self:addSubmenu("menus", "Menus")
    self:addItem("Inventory", "left", function() self:launchScene(scenes.Inventory) end)
    self:addItem("Options", "left", function() self:launchScene(scenes.Options) end)
    self:addItem("Tests", "left", function() self:launchScene(scenes.TestMenu) end)
    self:switch("main")
    self:addItem("Exit game", "left", function() self:exitGame() end)
    self:getFocus()
end

function MainMenu:launchScene(scene, arg1, arg2, arg3, arg4, arg5)
    local args = {arg1, arg2, arg3, arg4, arg5}
    core.screen:startTransition(defTransitions.circle, defTransitions.default, function() scene(args) end, 424/2, 240/2)
end

function MainMenu:exitGame()
    love.event.quit("000")
end

return MainMenu
