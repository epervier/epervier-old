return {
  ["imagefonts"] = {
    {"medium",    "assets/fonts/medium"}
  },
  ["sfx"] = {
    {"navigate",  "assets/sfx/menu_move.mp3"},
    {"confirm",   "assets/sfx/menu_confirm.mp3"},
    {"cancel",    "assets/sfx/menu_error.mp3"},
  }
}
