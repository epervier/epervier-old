return {
  MainMenu = require "scenes.mainmenu",
  Inventory = require "scenes.menus.inventory",
  Options = require "scenes.menus.options",
  MovePlayer = require "scenes.gameplay.moveplayer",
  MovePlayer3D = require "scenes.gameplay.moveplayer3D",
  Action3D = require "scenes.gameplay.action3D",
  Plateformer = require "scenes.gameplay.plateform"
}
