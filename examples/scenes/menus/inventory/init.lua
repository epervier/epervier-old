-- scenes/inventory :: a basic inventory example

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Scene       = require "framework.scenes"
local Inventory   = Scene:extend()

local HListBox  = require "framework.scenes.gui.menus.hlistbox"
local FloxBox      = require "framework.scenes.gui.menus.flowbox"
local Widget    = require "framework.scenes.gui.menus.widgets"

local InventoryWidget = Widget.Text:extend()
local ItemWidget      = Widget.Text:extend()
local ExitWidget      = Widget.Text:extend()

function Inventory:new()
  Inventory.super.new(self)

  HListBox(self.menusystem, "main", 42, 32, 424-84, 32, 5)
  self:addSubMenu("weapon", "weapons")
  self:addSubMenu("shield", "shields")
  self:addSubMenu("stuff",  "stuff")
  ExitWidget(self, "main", "exit")

  self:addItem("weapon", 3)
  self:addItem("weapon", 2)

  self:addItem("shield", 2)
  self:addItem("shield", 1)

  self.menusystem:lockMenu("main")

  self.menusystem:switchMenu("main")
  self.menusystem:setMenuActivity("weapon", true)
end

-- MENU FUNCTION
-- Functions that serve the handling of menus

function Inventory:addMenu(name, nobackbutton)
  local w, h = 424 - 84, 240 - 48
  local x, y = 42,       64  + 4
  local widgetw, widgeth = w / 24, h / 24
  FloxBox(self.menusystem, name, x, y, w, h, widgetw, widgeth)
end

function Inventory:addSubMenu(name, fullname)
  self:addMenu(name)
  InventoryWidget(self, "main", name, fullname)
end

function Inventory:addItem(submenu, id)
  ItemWidget(self, submenu, id)
end

function Inventory:draw()
  love.graphics.setColor(.3,  .1, .4, 1)
  love.graphics.rectangle("fill", 0, 0, 424, 240)
end

-- WIDGETS
-- Widgets used by menus

-- Submenu widget :: go to a submenu

function InventoryWidget:new(scene, menu, newmenu, fullname)
  self.scene = scene
  local widgetmenu  = self.scene.menusystem.menus[menu]
  local font        = "medium"
  self.newmenu      = newmenu
  local label       = core.lang:translate("inventory", fullname)
  InventoryWidget.super.new(self, widgetmenu, font, label)
end

function InventoryWidget:action(source)
  self.scene.menusystem:switchMenu(self.newmenu)
  if (source == "pointer") then
    self.scene.menusystem.menus["main"]:getFocus()
  end
end

-- Scene widget :: switch scene

function ItemWidget:new(scene, menu, id)
  self.scene = scene
  local widgetmenu  = self.scene.menusystem.menus[menu]
  local font        = "medium"
  ItemWidget.super.new(self, widgetmenu, font, id)
end

function ItemWidget:action()

end

-- Exit widget

function ExitWidget:new(scene, menu)
  self.scene = scene
  local widgetmenu  = self.scene.menusystem.menus[menu]
  local font        = "medium"
  local label       = core.lang:translate("commons", "exit")
  ExitWidget.super.new(self, widgetmenu, font, label)
end

function ExitWidget:action(source)
  core.scenemanager:setStoredScene("mainmenu")
end

return Inventory
