local TextMenu   = require "framework.scenes.gui.textmenu"
local OptionMenu = TextMenu:extend()

local consts = require "scenes.menus.options.consts"

function OptionMenu:new(font)
    local screenWidth, _ = core.screen:getDimensions()
    local w = screenWidth - (consts.BORDERS_W*2)
    local x = consts.BORDERS_W
    local y = consts.BORDERS_H
    OptionMenu.super.new(self, consts.OPTION_MENU, font, x, y, w, consts.NBR_ELEMS, 8)
end

return OptionMenu