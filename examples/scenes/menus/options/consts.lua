local consts = {}

consts.OPTION_MENU = "optionMenu"
consts.BORDERS_W = 96
consts.BORDERS_H = 24
consts.NBR_ELEMS = 8

return consts