-- scenes/options :: a basic example of how to handle birb options in a menu

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Scene     = require "framework.scenes"
local OptionsMenu  = Scene:extend()

local Widgets   = require "scenes.menus.options.widgets"
local Menu      = require "scenes.menus.options.menu"

local consts = require "scenes.menus.options.consts"

function OptionsMenu:start()
  OptionsMenu.super.new(self)
  
  assets:playMusic("options")

  self.menu = Menu("medium");

  self:addSubMenu("video", "video")
  Widgets.Resolution(self, "video")
  Widgets.Switch("fullscreen")
  Widgets.Switch("borders")
  Widgets.Switch("vsync")

  self:addSubMenu("audio", "audio")
  Widgets.Audio("sfx")
  Widgets.Audio("music")

  self:addSubMenu("langs", "langs")

  self:setLanguageMenu()

  self:addSubMenu("inputs", "inputs")

  self:addPlayerMenus()

  local menu = self.menusystem.menus[consts.OPTION_MENU]
  menu:switch("main")

  Widgets.Exit(self, "main")
  self.menusystem:switchMenu(consts.OPTION_MENU)

  self.menusystem:setSoundFromSceneAssets("navigate")

  self.keyDetector = {}
  self.keyDetector.widget = nil
end

-- MENU FUNCTION
-- Functions that serve the handling of menus

function OptionsMenu:addMenu(name, nobackbutton)
  Menu("medium")
end

function OptionsMenu:addSubMenu(name, fullname)
  local menu = self.menusystem.menus[consts.OPTION_MENU]
  menu:addSubmenu(name, core.lang:translate("options", fullname), "main")
end

function OptionsMenu:addPlayerMenus()
  for key, _ in pairs(core.input.data[1].keys) do
    -- FIXME: make sure that you can use the order you want for the keys list
    -- instead of a random one
    Widgets.Key(key)
  end
end

function OptionsMenu:addScene(submenu, scene, fullname)
  Widgets.Dummy(self, submenu, fullname)
end

function OptionsMenu:setLanguageMenu()
  for i,lang in ipairs(core.lang.data.available) do
    Widgets.Lang(lang)
  end
end

function OptionsMenu:changeKey(widget)
  self.keyDetector.isActive = true
  self.keyDetector.widget = widget
end

function OptionsMenu:keypressed( key )
  if (self.keyDetector.isActive) then
    self.keyDetector.widget:receiveKey( key )
    self.menusystem:activate()
    self.keyDetector.isActive = false
  end
end

function OptionsMenu:draw()
  love.graphics.setColor(.3,  .1, .4, 1)
  love.graphics.rectangle("fill", 0, 0, 424, 240)
  utils.graphics.resetColor()
end

return OptionsMenu
