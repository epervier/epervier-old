return {
    [1] = {
      type = "keyboard",
      keys = {
        ["left"]  = "q",
        ["right"] = "d",
        ["up"]    = "z",
        ["down"]  = "s",
        ["A"]     = "lshift",
        ["B"]     = "lctrl",
        ["C"]     = "w",
        ["start"] = "space",
        ["select"] = "c"
      }
    },
    [2] = {
      type = "keyboard",
      keys = {
        ["left"]  = "left",
        ["right"] = "right",
        ["up"]    = "up",
        ["down"]  = "down",
        ["A"]     = "1",
        ["B"]     = "2",
        ["C"]     = "3",
        ["start"] = "return",
        ["select"] = "space"
      }
    },
    [3] = {
      type = "keyboard",
      keys = {
        ["left"]  = "f",
        ["right"] = "h",
        ["up"]    = "t",
        ["down"]  = "g",
        ["A"]     = "v",
        ["B"]     = "b",
        ["C"]     = "n",
        ["start"] = "-",
        ["select"] = "/"
      }
    },
    [4] = {
      type = "keyboard",
      keys = {
        ["left"]  = "k",
        ["right"] = "m",
        ["up"]    = "o",
        ["down"]  = "l",
        ["A"]     = ",",
        ["B"]     = ";",
        ["C"]     = ":",
        ["start"] = "*",
        ["select"] = "!"
      }
    },
  }
