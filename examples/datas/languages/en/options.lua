return {
  ["inputs"]  = "Inputs",
  ["video"]   = "Video",
  ["audio"]   = "Audio",
  ["langs"]   = "Languages",
  ["resolution"]  = "Resolution",
  ["fullscreen"]  = "Fullscreen",
  ["borders"]     = "Borders",
  ["vsync"]       = "V-Sync",
  ["back"]        = "Back",
}
