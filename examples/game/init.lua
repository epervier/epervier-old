local GameSystem = require "framework.gamesystem"
local Game = GameSystem:extend()

function Game:new()
  Game.super.new(self)
  self.currentSlot = 1
end

return Game
