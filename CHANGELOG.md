# CHANGELOG

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

- Project renamed to Epervier Framework and rebased entirely on Sonic Radiance codebase

- New loading system

### Added

- Add a gamesystem module

- **core/debug:** new logging framework

- **camera+maps:** add a way to add padding to map limits

### Changed

- **world:** extract map module from the world module

- **core/input:** extract virtualpads from the input controller

- **core:** The core is now separated from the rest of the framework

- **core+assets:** Music are now managed directly by the core

## [0.6.0] - 2019-07-20

- Meta: Add proper crediting

### Added

- **actor2D:** Make hitbox modifiable.

- **actor2D:** Add multiple hitbox support.

- **assets:** Add a way to get current relative frame

- **assets:** Add more wrapper around animator functions in Sprite

- **world:** Add more wrapper around sprite functions in BaseActor

- **assets:** Add a new getCurrentAnimation function

- **world:** Add a way to automatically load hitbox from a file

- **utils:** Add basic table functions

- **camera:** Add two new camera types: "middle" and "zoom".

- **world:** Add a fake 3D world, à la Zelda or BeatThemAll, complete with shadow support

- **examples:** Test all player number and camera types

- **core.screen:** Add a way to get coordinate and scale

### Changed

- **world2D:** Use a list for bodies (hitboxes, etc) and one other for actors

- **world2D:** Make the hitbox an object, owned by the actor

- **actor:** Rename all function related to YGravity to just \*Gravity

- **world:** Separate "queryRect()" into two functions

- **world:** Make object creation more customizable by worlds

- **camera:** Make mode configuration configurable

- **examples:** Rename "movable" to "topdown"

- **camera:** Brand new camera system based on canvases instead of just transforms

- **core:** Activate vsync by default.

- **examples:** Put together all assets.

### Fixed

- **world:** Remove a forgotten camera debug function

- **examples:** Add missing translations

- **camera:** Fix onscreen coordinates by using new core.screen func

- **camera:** Fix camera's drawing in other resolution mode

- **world:** Don't draw unused sti objectlayer

### Removed

- **actor:** Remove all function related to XGravity

- **examples:** Remove basic examples

- **libs:** Remove hump.camera

## [0.5.1] - 2019-06-21

### Fixed

- **examples:** Add missing HUD example for one-player plateformer

- **world:** Separate getting dimensions of the internal and advertised view.

- **world:** use right camera internal coordinate

## [0.5.0] - 2019-06-16

- Meta: Add a Code of Conduct

- Meta: Add a Changelog

### Added

- **world:** Add an activity system

- **assets:** Add an activity system

- **example/plateformer:** add a basic pause button

- **scene**: Add functions to add stuf before and after `update` and `draw()`

- **actor**: Add functions to add stuf before and after `update` and `draw()`

- **world/camera:** Add support for Head-Up-Display

- **world:** Support batching actor loading

- **world:** Add `Actor.creationID` to keep the order in which actors have been created

- **world:** Add `Actor.depth` to keep the order in which actors are drawn

- **menusystem;** Add a way to deactivate assets and the world when activating the menu

- **world:** Add a reset function

- **examples/plateform:** Add a pause menu

### Changed

- **world:** automatize world integration in a scene

- **world:** only visible actors are drawn

### Fixed

- **core.input:** use the right argument in `core.input:flushKeys()`'s loop

- **scene:** use `core.input:flushKeys()` in `Scene:flushKeys()` instead of `core.input:flushSourceKeys()`

## [0.4.0] - 2019-05-06

- Add Bump2D-handled world system

### Added

- **world:** Add a `World2D` world object using Bump2D as a backend

- **world/actor:** Support bump2D filter

- **loveutils:** `love.math.toZero` substract a variable to another to zero

- **assets:** Add batch loading of assets

- **examples:** Add batch asset files

- **examples/plateformer:** Add a plateformer example

- **world:** Add a splitscreen-compatible camera system powered by hump.camera

- **examples:** Add cameras

- **world:** Support object loading from Tiled

- **world/actor:** Add sprite support in Actor2D

- **assets:** Add support for sprites origins in tileset and animations

- **world:** Add `World:initActors()` to init Actors

- **world:** automatize multiple character handling

- **loveutils:** `utils.math.between(num, min, max)` make sure that a number is between two value

- **world:** Add a gravity system

- **world/actors:** `Actor:destroy()` destroy the actor

- **world/actors:** Add a basic timer system to actors

- **world/actors:** Create a parent `BaseActor` that'll serve as a parent for Actor2D and other future actors

- **assets:** add getDimensions() to tileset, sprite and animators

- **world:** add a generic gfx actor

### Changed

- **examples/moveplayer:** move to World2D

- **world:** separate map loading from level initialization

- **examples/moveplayer:** use a different map

- **examples/moveplayer:** Use this example to show multiplayer capabilities

## [0.3.0] - 2019-04-20

- General cleanups and reorganisation of the base code to be more legible

- Add translation

- Some internal improvement to how gamecore works

### Added

- **examples:** Basic Inventory menu

- **menusystem:** Horizontal listbox menu

- **menusystem:** Lock/visiblity management functions

- **examples/mainmenu:** Add a back and exit widget

- **world:** Embed librairies

- **world:** Add actors managing functions

- **world:** Map loading system using STI

- **examples:** Add a movable multi-player example

- **world:** Add a basic player system directly inside world system

- **menusystem:** `Widget.creationID` contain the creation order of every menu widget

- **menusystem:** `Widget.order` handle the order of the widget in the menu

- **world:** add `xsp` and `ysp` variable to handle speed to the actor object

- **examples:** add an option menu

- **menusystem:** `Widget:invalidateCanvas()` to make a canvas need a redraw

- **core.lang:** Get the translation data instead of directly putting them in core.lang.datas

- **core.options:** Add controller variable

- **core.options:** `core.options:setInputKey(sourceid, padkey, key)`

- **scenes:** `Scene:keypressed( key, scancode, isrepeat )` and `Scene:keyreleased( key )` callbacks added

- **menusystem:** Add a global activity flag

- **examples/options:** Key customization handling

- **examples:** Add music and SFX

- **menusystem:** Add `Menu:resetSound()` to entirely reset the sound values of a menu

- **menusystem:** Add `Menu:setSoundFromSceneAssets(name)` to simplify sound managin

- **menusystem:** Add sound batch management function

### Changed

- **menusystem:** Separate activity and visiblity lock

- **menusystem:** Add as an argument to widgets actions if the widget have been interacted by a pointer or a key

- **examples:** Reorganize folder

- **world:** Make world system need an actorlist

- **core.inputs:** /!\BREAKING: Use a multi-source system for the input system, and adapt different module to use it

- **core.lang:** /!\BREAKING: New language metadata format and complete rework of the translation system

- **core:** directly handle love2D's callbacks inside gamecore

- **menusystem:** rename `Menu:playSelectSound()` to `Menu:self:playSelectSound()`

- **menusystem:** Internally link the whole menusystem to a scene

### Removed

- `MenuSystem.TextMenu` wasn't used since a long time, and was a menu that modified the widgets.

- `MenuSystem:keyreleased(key, code)` was just dead code

- `core.lang:getStringList(library, file)` is replaced in the new translation system

### Fixed

- core: load core.lang

## [0.2.0] - 2019-06-04

### Added

- **examples:** Use some text menus in the example menu

- **examples:** Test entity system

- **examples:** Add a main menu to the examples

- **menusystem:** Add functions to menusystem to set visibility to menus

- **menusystem:** Add functions to menusystem to switch current menu

- **menusystem:** Add depth system to menus

- **world:** Add world and actor system

### Changed

- **scene:** rework input locking system to add a timeout

### Fixed

- **loveutils:** Fix a crash in `utils.math.pointDistance`

## [0.1.0] - 2019-18-03

### Added

- **examples:** First two basic examples to show the capability of gamecore

- **scenes:** Start using a wrapper around `core.scenemanager.currentScene` to set the new Scene

- **scenes:** Add a storage system inside `core.scenemanager` to be able to keep previous scene

- **menusystem:** Basic menu examples to test menu capabilities

- **assets:** Add a mask system to `Texture`, `Tileset` and `Animation`

### Changed

- **scene:** `Scene` now registerd themselves

- **assets:** `Texture` assets are now a custom object instead of directly using love's texture.

### Fixed

## [0.0.1] - 2019-16-03

- Initial release based on a standalone version of Imperium Porcorum's Core.
