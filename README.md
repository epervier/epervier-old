# Épervier Framework

The Épervier Framework is an RPG-oriented framework for Love2D. It aim to work as a set of managers to automatically handle inputs, screen, and several utilities to make game developpement easier and less repetitive. It's also specialized in game with RPG mechanics, with functions to serialize/deserialize easily datas.

Épervier use [Classic](https://github.com/rxi/classic/) as its base Object.

## Core features

Épervier provide a lot of feature that can be usefull to create an RPG. This is a non-exhaustive list of what the framework can do :

- Scene system to be able to change your gameplay easily, with transitions
- Easy data loading and parsing via the core.data module
- A save system using data serialization
- Tweening and time support via tween.lua
- A world system with support for camera, multiple hitbox per actor, and two type of physics (bump2D and bump3D) and tiled map loading (via sti.lua)
- A GUI system to make your game more easily have HUD and menus
- Several utilities functions

## How to load Épervier

The framework must be located in the `framework/` folder to work. After that, all you have to do is to load a gamecore based engine and then.

Note : the `framework`, `core`, `game` and `utils` global namespaces will be used by the framework.

```lua
require "framework"

function love.load(args)
  framework.start("game", args)
end

```

### Launch in debug mode

To launch in debug mode, the love2D game must be launched with a DEBUGLEVEL bigger than 1, for instance :

```sh
love ./examples DEBUGLEVEL=4
```